﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeSolidBlock : MonoBehaviour
{
    public GridAttributes gridBelow;
    // Start is called before the first frame update
    void Start()
    {

        GetComponentInChildren<MeshRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        gridBelow = ReferenceMaster.instance.gridSystem.GetMyGridBelow(transform.position);
        if (gridBelow!= null)
        gridBelow.GridBlock();
    }

    private void OnDisable()
    {
        if (gridBelow != null)
            gridBelow.GridTBlockRemove();
    }
}
