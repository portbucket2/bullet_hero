﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFacingDirectionControl : MonoBehaviour
{
    public GameObject enemyMesh;
    public EnemyPathFinding enemyPathFinding;
    Quaternion rotFinal;
    Quaternion rotTarget;
    Quaternion rotCur;
    DroneBehavior droneBehavior;
    HumveeBehavior humveeBehavior;
    MoverToAnyPoint moverToAnyPoint;
    BossSwatBehavior bossSwatBehavior;
    float swatAimFac;
    private void Awake()
    {
        enemyPathFinding = GetComponent<EnemyPathFinding>();
        if (enemyPathFinding.drone)
        {
            droneBehavior = GetComponent<DroneBehavior>();
        }
        if (enemyPathFinding.humvee)
        {
            humveeBehavior = GetComponent<HumveeBehavior>();
        }
        if (enemyPathFinding.bossSwat)
        {
            moverToAnyPoint = GetComponent<MoverToAnyPoint>();
            bossSwatBehavior = GetComponent<BossSwatBehavior>();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(enemyMesh == null)
        {
            return;
        }

        if (enemyPathFinding.drone && enemyPathFinding.initialized)
        {
            if (droneBehavior.target)
            {
                rotTarget = Quaternion.LookRotation(droneBehavior.target.transform.position - enemyMesh.transform.position, enemyMesh.transform.up);
            }
            else
            {
                rotTarget = Quaternion.LookRotation(ReferenceMaster.instance.playerWeaponControl.transform.position - enemyMesh.transform.position, enemyMesh.transform.up);
            }
        }
        else if (enemyPathFinding.humvee && enemyPathFinding.initialized)
        {
            if (humveeBehavior.target)
            {
                //rotTarget = Quaternion.LookRotation(humveeBehavior.target.transform.position - enemyMesh.transform.position, enemyMesh.transform.up);
            }
            else
            {
                //rotTarget = Quaternion.LookRotation(ReferenceMaster.instance.playerWeaponControl.transform.position - enemyMesh.transform.position, enemyMesh.transform.up);
            }
            return;
        }
        else if (enemyPathFinding.bossSwat && enemyPathFinding.initialized)
        {
            if (moverToAnyPoint.walk)
            {
                rotTarget = Quaternion.LookRotation(moverToAnyPoint.target.transform.position - enemyMesh.transform.position, enemyMesh.transform.up);
            }
            else
            {
                rotTarget = Quaternion.LookRotation(ReferenceMaster.instance.playerWeaponControl.transform.position - enemyMesh.transform.position, enemyMesh.transform.up);
            }
            
            rotTarget = BossSwatRotLimitMaintain(rotTarget);
            //bossSwatBehavior.swatUpperBody.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
            //Debug.Log(bossSwatBehavior.swatUpperBody.rotation.ToEulerAngles() * Mathf.Rad2Deg);
        }
        else
        {
            if (enemyPathFinding.walk && !enemyPathFinding.PausedOrNot() && enemyPathFinding.target)
            {
                if(enemyPathFinding.target.transform.position != enemyMesh.transform.position)
                rotTarget = Quaternion.LookRotation(enemyPathFinding.target.transform.position - enemyMesh.transform.position, enemyMesh.transform.up);
            }
            else
            {
                rotTarget = Quaternion.LookRotation(ReferenceMaster.instance.playerWeaponControl.transform.position - enemyMesh.transform.position, enemyMesh.transform.up);
            }
        }
        
        enemyMesh.transform.rotation = Quaternion.Lerp(enemyMesh.transform.rotation, rotTarget, Time.deltaTime * 10f);

    }
    private void LateUpdate()
    {
        if (enemyPathFinding.bossSwat && enemyPathFinding.initialized)
        {
            Quaternion gunRotTarget = Quaternion.LookRotation(ReferenceMaster.instance.playerWeaponControl.transform.position - enemyMesh.transform.position, enemyMesh.transform.up)
                    * Quaternion.Euler(bossSwatBehavior.swatUpperBodyTargetOffset);
            Quaternion realRot = bossSwatBehavior.swatUpperBody.rotation;
            if (moverToAnyPoint.walk)
            {
                
                //gunRotTarget = Quaternion.LookRotation(ReferenceMaster.instance.playerWeaponControl.transform.position - enemyMesh.transform.position, enemyMesh.transform.up) 
                //    * Quaternion.Euler(bossSwatBehavior.swatUpperBodyTargetOffset);
                //
                swatAimFac += Time.deltaTime * 3;
                if(swatAimFac >= 1)
                {
                    swatAimFac = 1;
                }
                //bossSwatBehavior.swatUpperBody.rotation = Quaternion.Lerp(bossSwatBehavior.swatUpperBody.rotation, gunRotTarget, Time.deltaTime * 10f);
                //bossSwatBehavior.swatUpperBody.rotation = gunRotTarget;
                
            }
            else
            {
                //rotTarget = Quaternion.LookRotation(ReferenceMaster.instance.playerWeaponControl.transform.position - enemyMesh.transform.position, enemyMesh.transform.up);
                //gunRotTarget = realRot;
                swatAimFac -= Time.deltaTime * 3;
                if (swatAimFac <= 0)
                {
                    swatAimFac = 0;
                }
            }
            //rotTarget = BossSwatRotLimitMaintain(rotTarget);
            bossSwatBehavior.swatUpperBody.rotation = Quaternion.Lerp(realRot, gunRotTarget,swatAimFac);
            //Debug.Log(bossSwatBehavior.swatUpperBody.rotation.ToEulerAngles() * Mathf.Rad2Deg);
        }
    }

    public void EnemyDeathWaitTillAnimation(float time)
    {
        if (enemyMesh == null)
        {
            return;
        }
        enemyMesh.transform.SetParent(ReferenceMaster.instance.ArmyOfDeadHolder.transform);
        enemyPathFinding.enemyAnimatorController.DeathEnemyAnim();
        Destroy(enemyMesh.gameObject,time);
    }

    public Quaternion BossSwatRotLimitMaintain(Quaternion rotCur)
    {
        Quaternion rot = Quaternion.identity;
        float rotMax = 40;
        Vector3 rotAngle = Quaternion.ToEulerAngles(rotCur) * Mathf.Rad2Deg;
        if(rotAngle.y < rotMax && rotAngle.y > -rotMax)
        {
            if(rotAngle.y >= 0)
            {
                rotAngle.y = rotMax;
            }
            else
            {
                rotAngle.y = -rotMax;
            }
        }

        //Debug.Log(rotAngle);
        return rot = Quaternion.Euler(rotAngle);
    }
    
}
