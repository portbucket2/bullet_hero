﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BossTankRotationControl : MonoBehaviour
{
    MoverToAnyPoint moverToAnyPoint;
    public GameObject bodyToRotate;
    public bool rotate;
    public bool turretAim;
    public GameObject tankTurret;
    public float rotNeeded;
    public Action ActionRotStart;
    public Action ActionRotEnd;
    EnemyPathFinding enemyPathFinding;
    // Start is called before the first frame update
    void Start()
    {
        moverToAnyPoint = GetComponent<MoverToAnyPoint>();
        moverToAnyPoint.ActionStationLeave += CheckPauseToRotateNeededOrNot;
        moverToAnyPoint.ActionDestinationReach += TurretAim;

        enemyPathFinding = GetComponent<EnemyPathFinding>();
    }

    // Update is called once per frame
    void Update()
    {
        if (rotate)
        {
            RotateTheBody();
        }
        if (turretAim)
        {
            TurretAim();
        }

        if(enemyPathFinding.walk && !enemyPathFinding.initialized)
        {
            bodyToRotate.transform.rotation = Quaternion.LookRotation(enemyPathFinding.target.transform.position - bodyToRotate.transform.position, Vector3.up );
        }
    }

    public void RotateTheBody()
    {
        Quaternion oldRot = bodyToRotate.transform.rotation;
        Quaternion targetRot = Quaternion.LookRotation(moverToAnyPoint.target.transform.position  - bodyToRotate.transform.position, Vector3.up);
        bodyToRotate.transform.rotation = Quaternion.RotateTowards(bodyToRotate.transform.rotation, targetRot, Time.deltaTime * 140f);
        Quaternion newRot = bodyToRotate.transform.rotation;

        //rotNeeded = (Quaternion.ToEulerAngles(newRot) - Quaternion.ToEulerAngles(oldRot)).y * Mathf.Rad2Deg *50;
        if (bodyToRotate.transform.rotation == targetRot)
        {
            moverToAnyPoint.ResumeMoving();
            rotate = false;
            ActionRotEnd?.Invoke();
        }

        TurretBackToNormalRot();
    }
    public void TurretAim()
    {
        if(!turretAim)
        {
            turretAim = true;
        }
        Quaternion targetRot = Quaternion.LookRotation(ReferenceMaster.instance.playerWeaponControl.transform.position - tankTurret.transform.position, Vector3.up);
        tankTurret.transform.rotation = Quaternion.RotateTowards(tankTurret.transform.rotation, targetRot, Time.deltaTime *200f);
        if (tankTurret.transform.rotation == targetRot)
        {
            turretAim = false;
        }
        Debug.Log("TurretAimm");
    }
    public void TurretBackToNormalRot()
    {
        Quaternion rot = Quaternion.Euler(new Vector3(0, 0, 0));
        
        if (tankTurret.transform.localRotation != rot)
        {
            tankTurret.transform.localRotation = Quaternion.RotateTowards(tankTurret.transform.localRotation, rot, Time.deltaTime * 200f);
        }
    }
    public void CheckPauseToRotateNeededOrNot()
    {
        //bool needed = true;
        Quaternion targetRot = Quaternion.LookRotation(moverToAnyPoint.target.transform.position - bodyToRotate.transform.position, Vector3.up);
        if (bodyToRotate.transform.rotation != targetRot)
        {
            //needed = true;
            rotate = true;
            moverToAnyPoint.PauseMoving();
        }

        rotNeeded = (Quaternion.ToEulerAngles(targetRot) - Quaternion.ToEulerAngles(bodyToRotate.transform.rotation)).y * Mathf.Rad2Deg ;
        //rotNeeded = rotNeeded % 180;
        ActionRotStart?.Invoke();
        ///Debug.Log("Working " + needed);
        //return needed;
    }
}
