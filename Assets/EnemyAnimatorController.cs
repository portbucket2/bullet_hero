﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimatorController : MonoBehaviour
{
    public Animator anim;
    // Start is called before the first frame update
    private void Awake()
    {
        anim = GetComponentInChildren<Animator>();
        GetComponent<EnemyPathFinding>(). movingEndedAction += IdleEnemyAnim;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RunEnemyAnim()
    {
        if(anim != null)
            anim.SetBool("run",true);
    }
    public void IdleEnemyAnim()
    {
        if (anim != null)
            anim.SetBool("run", false);
    }
    public void AttackEnemyAnim()
    {
        if (anim != null)
            anim.SetTrigger("attack");
    }
    public void DeathEnemyAnim()
    {
        if (anim != null)
            anim.SetTrigger("death");
    }
    public void UnCoverEnemyAnim()
    {
        if (anim != null)
            anim.SetBool("uncovered", true);
    }
    public void CoverEnemyAnim()
    {
        if (anim != null)
            anim.SetBool("uncovered", false);
    }
    public void AimUpEnemyAnim()
    {
        if (anim != null)
            anim.SetBool("aim", true);
    }
    public void AimDownEnemyAnim()
    {
        if (anim != null)
            anim.SetBool("aim", false);
    }
}
