﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerValuesAssignHelper : MonoBehaviour
{
    // Start is called before the first frame update
    PlayerWeaponControl playerWeaponControl;
    PlayerShieldFuntion playerShieldFuntion;
    PlayerHealth playerHealth;
    void Start()
    {
        playerWeaponControl = ReferenceMaster.instance.playerWeaponControl;
        playerShieldFuntion = ReferenceMaster.instance.playerShieldFuntion;
        playerHealth = ReferenceMaster.instance.playerHealth;

        PlayerWeaponType playerWeaponType = ReferenceMaster.instance.gameValuesTwaekHelper.playerValues.weaponType;
        PlayerWeaponChange(playerWeaponType);
        GetComponent<PlayerHealth>().SetPlayeinitialHealth(ReferenceMaster.instance.gameValuesTwaekHelper.playerValues.health);
        GetComponent<PlayerShieldFuntion>().SetShieldInitialHealthMax(ReferenceMaster.instance.gameValuesTwaekHelper.playerValues.shieldHealth);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayerWeaponChange(PlayerWeaponType playerWeaponTyp)
    {
        //if(ReferenceMaster.instance.gameValuesTwaekHelper.playerValues.weaponType != playerWeaponTyp)
        //{
        //    ReferenceMaster.instance.gameValuesTwaekHelper.ChangePlayerWeaponType(playerWeaponTyp);
        //}

        
        GameValuesTwaekHelper.PlayerWeapon weapon = ReferenceMaster.instance.gameValuesTwaekHelper.playerWeapons[(int)playerWeaponTyp];
        playerWeaponControl.SetPlayerFireInterval(weapon.attackPerSec);
        playerWeaponControl.SetBulletDamage(weapon.damage);
        playerWeaponControl.SetBulletSpeed(weapon.bulletSpeed);
        playerWeaponControl.playerCurrentWeapon = playerWeaponTyp;

        
    }

    public void CardFunctionAssaultRifle()
    {
        
        PlayerWeaponType playerWeaponType = playerWeaponControl.playerCurrentWeapon;
        if(playerWeaponType != PlayerWeaponType.AssaultRifle)
        {
            PlayerWeaponChange(PlayerWeaponType.AssaultRifle);
        }
        else
        {
            playerWeaponControl.IncreaseBulletDamageByPercentage(25);
        }
        
    }
    public void CardFunctionRPG()
    {
        
        PlayerWeaponType playerWeaponType = playerWeaponControl.playerCurrentWeapon;
        if (playerWeaponType != PlayerWeaponType.Rpg)
        {
            PlayerWeaponChange(PlayerWeaponType.Rpg);
        }
        else
        {
            playerWeaponControl.IncreaseBulletDamageByPercentage(25);
        }

    }
    public void CardFunctionDamageIncrease25()
    {
        
        playerWeaponControl.IncreaseBulletDamageByPercentage(25);

    }
    public void CardFunctionFireRateIncrease25()
    {

        playerWeaponControl.FireRateIncreaseBy(25);

    }
    public void CardFunctionShieldIncrease50()
    {

        playerShieldFuntion.ShieldIncreaseBy(50);

    }
    public void CardFunctionHelathIncrease50()
    {

        playerHealth.PlayerHealthIncreaseBy(50);

    }
    public void CardFunctionHealFull()
    {

        playerHealth.PlayerHealthFullHeal();

    }
    public void CardFunctionHealHalf()
    {

        playerHealth.PlayerHealthHalfHeal();

    }
    public void CardFunctionReloadTime()
    {

        //playerHealth.PlayerHealthHalfHeal();

    }
}
