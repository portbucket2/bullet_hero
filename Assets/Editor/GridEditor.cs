﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(GridSystem))]
public class GridEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GridSystem gridSystem = (GridSystem)target;
        if(GUILayout.Button("Grid Create"))
        {
           
            gridSystem.MakeGround();
            
        }
        if (GUILayout.Button("Get Neighbours"))
        {
            
            gridSystem.GridUnitsAssignNeighbourGrids();
        }
        if (GUILayout.Button("left right entrance"))
        {

            gridSystem.IdentifyEntryGrids();
        }
    }
}
