﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimatorController : MonoBehaviour
{
    public Animator playerAnim;

    // Start is called before the first frame update
    void Start()
    {
        playerAnim = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //public void IdleEnemyAnim()
    //{
    //    if (playerAnim != null)
    //        playerAnim.SetBool("run", false);
    //}
    public void AttackPlayerAnim()
    {
        if (playerAnim != null)
            playerAnim.SetBool("attack" , true);
    }
    public void IdleCrouchPlayerAnim()
    {
        if (playerAnim != null)
            playerAnim.SetBool("attack", false);
    }
    public void DeathPlayerAnim()
    {
        if (playerAnim != null)
            playerAnim.SetTrigger("death");
    }
}
