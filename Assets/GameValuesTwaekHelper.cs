﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameValuesTwaekHelper : MonoBehaviour
{
    public PlayerValues playerValues;
    public PlayerWeapon[] playerWeapons;
    public EnemyValues melee;
    public EnemyValues assault;
    public EnemyValues rpg;
    public EnemyValues lmg;
    public EnemyValues humvee;
    public EnemyValues tripod;
    public EnemyValues drone;
    public EnemyValues bossSwat;
    public EnemyValues bossTank;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    [System.Serializable]
    public class EnemyValues
    {
        public string title;
        public int health;
        public int damage;
        public float attackPerSec;
        public float bulletSpeed;
        public float movementSpeed;
        
        public EnemyType enemyType;

        public float initialDeayToShoot;

    }
    [System.Serializable]
    public class PlayerValues
    {
        public string title;
        public int health;
        public int shieldHealth;
        public float bulletLifetime;
        
        //public float movementSpeed;

        public PlayerWeaponType weaponType;
        //public PlayerWeapon[] playerWeapons;

        public float initialDeayToShoot;

    }
    [System.Serializable]
    public class PlayerWeapon
    {
        public string title;
       
        public int damage;
        public float attackPerSec;
        
        public float bulletSpeed;
        public int clipSize;
        public float reloadTime;
        

        

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public EnemyValues GetEnemyValues(EnemyType enemyType)
    {
        
        EnemyValues enemyValues = null;
        switch (enemyType)
        {
            case EnemyType.Melee:
                {
                    enemyValues = melee;
                    break;
                }
            case EnemyType.Assault:
                {
                    enemyValues = assault;
                    break;
                }
            case EnemyType.RPG:
                {
                    enemyValues = rpg;
                    break;
                }
            case EnemyType.LMG:
                {
                    enemyValues = lmg;
                    break;
                }
            case EnemyType.Humvee:
                {
                    enemyValues = humvee;
                    break;
                }
            case EnemyType.Tripod:
                {
                    enemyValues = tripod;
                    break;
                }
            case EnemyType.Drone:
                {
                    enemyValues = drone;
                    break;
                }
            case EnemyType.BossSwat:
                {
                    enemyValues = bossSwat;
                    break;
                }
            case EnemyType.BossTank:
                {
                    enemyValues = bossTank;
                    break;
                }
        }
        return enemyValues;
    }
    public void ChangePlayerWeaponType(PlayerWeaponType weaponType)
    {
        playerValues.weaponType = weaponType;
    }

    
}
