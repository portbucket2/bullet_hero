﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LevelStart(int levelIndex)
    {
        ReferenceMaster.instance.gameStatesControl.GameStateChangeTo(GameStates.LevelStarted);

        ReferenceMaster.instance.gridSystem.RenewAllGrids();
        ReferenceMaster.instance.enemySpawner.EnemySpawnFunction(levelIndex);
        ReferenceMaster.instance.levelProgressionWallsInfo.LevelObstaclesEnable(levelIndex);

        
        ReferenceMaster.instance.touchInputSystem.EnableTouch();
    }
    public void LevelEnd()
    {
        ReferenceMaster.instance.gameStatesControl.GameStateChangeTo(GameStates.LevelEnded);
        ReferenceMaster.instance.levelEndCardFunctions. RandomThreeCardsAssign();
        
        //StartCoroutine( NextLevelCoroutine());
    }
    public IEnumerator LevelEndCoroutine(float t)
    {
        yield return new WaitForSeconds(t);
        LevelEnd();
    }
    public IEnumerator NextLevelCoroutine()
    {
        yield return new WaitForSeconds(3);
        LoadNextLevel();
    }

    public void LoadNextLevel()
    {
        ReferenceMaster.instance.gameManagementMain.NextLevel();
        LevelStart(ReferenceMaster.instance.gameManagementMain.levelIndex);
        
    }
}
