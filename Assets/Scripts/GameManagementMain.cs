﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagementMain : MonoBehaviour
{
    public int levelIndex;
    public int maxLevelIndex;
    public int score;
    private object l;

    // Start is called before the first frame update
    void Start()
    {
        ReferenceMaster.instance.gameStatesControl.GameStateChangeTo(GameStates.MenuIdle);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartGame()
    {
        ReferenceMaster.instance.uiDummy. PanelStart.gameObject.SetActive(false);
        ReferenceMaster.instance.uiDummy.UIUpdate();
        ReferenceMaster.instance.levelManager.LevelStart(ReferenceMaster.instance.gameManagementMain.levelIndex);
        //ReferenceMaster.instance.uiDummy.StartLevel(ReferenceMaster.instance.gameManagementMain.levelIndex);


    }
    public void NextLevel()
    {
        
        levelIndex += 1;
        if(levelIndex > maxLevelIndex)
        {
            levelIndex = 0;
        }
        //ReferenceMaster.instance.uiDummy.UIUpdate();
        //ReferenceMaster.instance.uiDummy.LevelClearedUIAppear();
        //ReferenceMaster.instance.uiDummy.StartLevel(levelIndex);
        //StartCoroutine(ReferenceMaster.instance.levelManager.LevelStartCoroutine());
    }
    public void RestartGame()
    {
        Time.timeScale = 1;
        Application.LoadLevel(0);
    }
    public void GameOver()
    {
        Time.timeScale = 0;
        ReferenceMaster.instance.uiDummy.GameOverFun();
    }
    public void ScoreUpdateBy(int i)
    {
        score += i;
    }
}
