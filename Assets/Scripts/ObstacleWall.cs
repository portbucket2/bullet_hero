﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleWall : MonoBehaviour
{
    //public GridAttributes[] gridExits;
    //public CoverPointAttributes coverPointPrefab;
    public List<GridAttributes> gridExits;
    public List<CoverPointAttributes> coverPoints;
    public int obstacleDistanceIndex;
    public List<GameObject> solidBlocks;
    public bool manualExitAssign;
    public bool newExitPointSyatem;
    //public List<GameObject> exitPoints;
    public int[] manualExitIndexes;
    public bool noGridOccupying;
    
    // Start is called before the first frame update
    void Start()
    {
        InitialWallOperations();


    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public CoverPointAttributes GetAnEmptyCoverPoint()
    {
        CoverPointAttributes coverEmpty = null;
        for (int i = 0; i < coverPoints.Count; i++)
        {
            if (!coverPoints[i].OccupiedOrNot())
            {
                coverEmpty = coverPoints[i];
                break;
            }
        }

        return coverEmpty;
    }
    public GridAttributes GetGridNearestExit(Vector3 pos)
    {
        GridAttributes gridExitt = null;
        if(gridExits.Count > 1)
        {
            float dis = Vector3.Distance(pos, gridExits[0].transform.position);
            gridExitt = gridExits[0];

            for (int i = 1; i < gridExits.Count; i++)
            {
                float d = Vector3.Distance(pos, gridExits[i].transform.position);
                if(d< dis)
                {
                    dis = d;
                    gridExitt = gridExits[i];
                }
            }
        }
        else
        {
            if (gridExits.Count > 0)
            {
                gridExitt = gridExits[0];
            }
        }
        
        return gridExitt;
    }
    public bool CheckItAnExitPointOrNot( GameObject gr)
    {
        bool res = false;
        int a = gr.GetComponent<GridAttributes>(). gridIndex;

        for (int i = 0; i < gridExits.Count; i++)
        {
            if (a == gridExits[i].gridIndex)
            {
                res = true;
                break;
            }
        }

        return res;
    }
    public int GetExitPointIndex(GridAttributes gr)
    {
        int res = -1;
        int a = gr.gridIndex;

        for (int i = 0; i < gridExits.Count; i++)
        {
            if (a == gridExits[i].gridIndex)
            {
                res = i;
                break;
            }
        }

        return res;
    }
    public GridAttributes GeNexttGridExit(GridAttributes currentExit)
    {
        GridAttributes nextExit = null;
        int curIndex = -1;
        if(gridExits.Count > 0)
        {
            for (int i = 0; i < gridExits.Count; i++)
            {
                if(currentExit.gridIndex == gridExits[i].gridIndex)
                {
                    curIndex = i;
                    Debug.Log("CurIndex " + i);
                    break;
                }
            }
            if(curIndex >= 0)
            {
                if((curIndex + 1) < gridExits.Count)
                {
                    nextExit = gridExits[curIndex + 1];
                    
                }
                else
                {
                    nextExit = gridExits[0];
                }
                Debug.Log("NextIndex " + nextExit.gridIndex);
            }
        }
        return nextExit;
    }
    public GridAttributes GeNexttGridExit(int currentExitIndex)
    {
        GridAttributes nextExit = null;
        int curIndex = -1;
        if (gridExits.Count > 0)
        {
            curIndex = currentExitIndex;
            if (curIndex >= 0)
            {
                if ((curIndex + 1) < gridExits.Count)
                {
                    nextExit = gridExits[curIndex + 1];

                }
                else
                {
                    nextExit = gridExits[0];
                }
                Debug.Log("NextIndex " + nextExit.gridIndex);
            }
        }
        return nextExit;
    }


    void OccupyGridsAndDetectExits()
    {
        if (!noGridOccupying)
        {
            List<GridAttributes> gridsUnder = new List<GridAttributes>();
            for (int i = 0; i < solidBlocks.Count; i++)
            {
                GridAttributes gridBelow = ReferenceMaster.instance.gridSystem.GetMyGridBelow(solidBlocks[i].transform.position);
                if (gridBelow != null)
                {
                    gridBelow.GridOccupy();
                    gridsUnder.Add(gridBelow);

                }
            }

            gridExits.Clear();
            if (gridsUnder[0] != null && gridsUnder[0].leftGrid != null)
            {
                if (!gridsUnder[0].leftGrid.OccupiedOrNot())
                {
                    gridExits.Add(gridsUnder[0].leftGrid);
                }
            }
            if (gridsUnder.Count > 1)
            {
                if (gridsUnder[gridsUnder.Count - 1].rightGrid != null)
                {
                    if (!gridsUnder[gridsUnder.Count - 1].rightGrid.OccupiedOrNot())
                    {
                        gridExits.Add(gridsUnder[gridsUnder.Count - 1].rightGrid);
                    }
                }
            }
        }
        
        if(manualExitAssign)
        {
            if (newExitPointSyatem)
            {
                return;
            }
            gridExits.Clear();
            for (int i = 0; i < manualExitIndexes.Length; i++)
            {
                
                GridAttributes gridExit = ReferenceMaster.instance.gridSystem.gridsPresent[manualExitIndexes[i]].GetComponent<GridAttributes>();
                gridExits.Add(gridExit);
            }
            return;
        }
        if (newExitPointSyatem)
        {
            gridExits.Clear();
            
            foreach(ExitPoint exit in GetComponentsInChildren<ExitPoint>())
            {
                //exitPoints.Add(exit.transform.gameObject);
                GridAttributes gridExit = ReferenceMaster.instance.gridSystem.GetMyGridBelow(exit.transform.position);
                gridExits.Add(gridExit);

                exit.GetComponentInChildren<MeshRenderer>().enabled = false;
            }


        }
        

        //for (int i = 0; i < gridsUnder.Count; i++)
        //{
        //
        //}
    }
    public void InitialWallOperations()
    {
        if (newExitPointSyatem)
        {
            coverPoints.Clear();

            foreach (CoverPointAttributes cover in GetComponentsInChildren<CoverPointAttributes>())
            {
                //exitPoints.Add(exit.transform.gameObject);
                //GridAttributes gridExit = ReferenceMaster.instance.gridSystem.GetMyGridBelow(cover.transform.position);
                coverPoints.Add(cover);
            }


        }

        for (int i = 0; i < coverPoints.Count; i++)
        {
            coverPoints[i].obstacleWallMine = this;
            coverPoints[i].coverIndex = i;
        }

        if (!noGridOccupying)
        {
            //OccupyGridsAndDetectExits();
        }
        OccupyGridsAndDetectExits();
        if (newExitPointSyatem)
        {
            solidBlocks.Clear();

            foreach (SolidBlock solid in  GetComponentsInChildren<SolidBlock>())
            {
                //exitPoints.Add(exit.transform.gameObject);
                //GridAttributes gridExit = ReferenceMaster.instance.gridSystem.GetMyGridBelow(cover.transform.position);
                solidBlocks.Add(solid.gameObject);
            }


        }
        for (int i = 0; i < solidBlocks.Count; i++)
        {
            solidBlocks[i].GetComponentInChildren<MeshRenderer>().enabled = false;
        }
        
    }
}
