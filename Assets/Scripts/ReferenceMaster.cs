﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReferenceMaster : MonoBehaviour
{
    public static ReferenceMaster instance;
    public PlayerWeaponControl playerWeaponControl;
    public TouchInputSystem touchInputSystem;
    public GridSystem gridSystem;
    public ObstaclesManager obstacleManager;
    public PlayerHealth playerHealth;
    public PlayerShieldFuntion playerShieldFuntion;
    public EnemySpawner enemySpawner;
    public GameManagementMain gameManagementMain;
    public UIDummy uiDummy;
    public LevelProgressionEnemyInfo LevelProgressionEnemyInfo;
    public LevelProgressionWallsInfo levelProgressionWallsInfo;
    public LevelManager levelManager;
    public LevelEndCardFunctions levelEndCardFunctions;
    public GameStatesControl gameStatesControl;
    public GameObject ArmyOfDeadHolder;
    public GameValuesTwaekHelper gameValuesTwaekHelper;
    public PlayerValuesAssignHelper playerValuesAssignHelper;


    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
