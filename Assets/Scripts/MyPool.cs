﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPool : MonoBehaviour
{
    public GameObject objToPool;
    public List<GameObject> objectPoolList;
    public GameObject poolHolder;
    public int initialCount;


    private void Awake()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        CreatePool();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CreatePool()
    {
        for (int i = 0; i < initialCount; i++)
        {
            GameObject go = Instantiate(objToPool, transform.position, transform.rotation);
            go.transform.SetParent(poolHolder.transform);
            go.name = objToPool.name + " "+ objectPoolList.Count;
            objectPoolList.Add(go);

            go.SetActive(false);
        }
    }

    public GameObject InstantiateFromPool(Vector3 pos, Quaternion rot)
    {
        GameObject go = null;
        if(objectPoolList.Count > 0)
        {
            go = objectPoolList[0].gameObject;
            objectPoolList.RemoveAt(0);
            go.transform.position = pos;
            go.transform.rotation = rot;
            go.SetActive(true);
        }
        
        

        return go;

    }
    public void DestroyBackToPool(GameObject go)
    {
        go.transform.SetParent(poolHolder.transform);
        
        objectPoolList.Add(go);

        go.SetActive(false);

    }
}
