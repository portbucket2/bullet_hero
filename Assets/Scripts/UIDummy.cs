﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UIDummy : MonoBehaviour
{
    //public Button ButtonStart;
    public GameObject PanelStart;
    public Text[] levelNoTexts;
    public Text nextLevelNoText;
    //public Action actionStartGame;
    public GameObject gameOverPanel;
    public Text[] scoreTexts;
    public GameObject levelClearedPanel;
    public GameObject levelClearedSubPanel;
    public GameObject levelEndCardsSubPanel;
    //public Button[] allButtons;
    public enum LevelClearedUiStateEnum
    {
        levelEndCards,levelEndPanel
    }
    // Start is called before the first frame update
    void Start()
    {
        ReferenceMaster.instance.gameStatesControl.ActionMenuIdle     += MenuIdleFun;     
        ReferenceMaster.instance.gameStatesControl.ActionGameStarted  += GameStartedFun  ;
        ReferenceMaster.instance.gameStatesControl.ActionGamePaused   += GamePausedFun   ;
        ReferenceMaster.instance.gameStatesControl.ActionLevelStarted += LevelStartedFun ;
        ReferenceMaster.instance.gameStatesControl.ActionLevelEnded   += LevelEndedFun    ;
        ReferenceMaster.instance.gameStatesControl.ActionGameOver += GameOverFun;

         //allButtons = GetComponentsInChildren<Button>();
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
    //public void StartLevel(int levelIndex)
    //{
    //    ReferenceMaster.instance.gridSystem.RenewAllGrids();
    //    ReferenceMaster.instance.enemySpawner.EnemySpawnFunction(levelIndex);
    //    ReferenceMaster.instance.levelProgressionWallsInfo.LevelObstaclesEnable(levelIndex);
    //
    //    levelClearedPanel.SetActive(false);
    //    ReferenceMaster.instance.touchInputSystem.EnableTouch();
    //}
    //public IEnumerator StartLevelCoroutine()
    //{
    //    yield return new WaitForSeconds(3);
    //    StartLevel(ReferenceMaster.instance.gameManagementMain.levelIndex);
    //}

    public void UIUpdate()
    {
        for (int i = 0; i < levelNoTexts.Length; i++)
        {
            levelNoTexts[i].text = "" + (ReferenceMaster.instance.gameManagementMain.levelIndex + 1);
        }
     
        for (int i = 0; i < scoreTexts.Length; i++)
        {
            scoreTexts[i].text =""+ ReferenceMaster.instance.gameManagementMain.score;
        }
        nextLevelNoText.text = "" + (ReferenceMaster.instance.gameManagementMain.levelIndex + 2);
    }
    public void LevelClearedUIAppear(LevelClearedUiStateEnum state)
    {
        
        ReferenceMaster.instance.touchInputSystem.DisableTouch();
        if (!levelClearedPanel.activeSelf)
        {
            levelClearedPanel.SetActive(true);
            levelClearedPanel.GetComponent<UiTransitionCustom>().TransitionRightOrLeftToCenter(-2000, 0, 0, 3);
        }
        
        switch (state)
        {
            case LevelClearedUiStateEnum.levelEndCards:
                {
                    levelClearedSubPanel.SetActive(false);
                    levelEndCardsSubPanel.SetActive(true);
                    
                    break;
                }
            case LevelClearedUiStateEnum.levelEndPanel:
                {
                    levelClearedSubPanel.SetActive(true);

                    
                    levelEndCardsSubPanel.GetComponent<UiTransitionCustom>().TransitionRightOrLeftToCenter(0,2000, 0, 3,null, true);
                    levelClearedSubPanel.GetComponent<UiTransitionCustom>().TransitionRightOrLeftToCenter(-2000,0, 0, 3, null);
                    //levelEndCardsSubPanel.SetActive(false);
                    break;
                }
        }
    }
    public void MenuIdleFun     ()
    {
        UIUpdate();
        gameOverPanel.SetActive(false);
        PanelStart.gameObject.SetActive(true);

        levelClearedPanel.SetActive(false);
    }
    public void GameStartedFun  () {
        UIUpdate();
    }
    public void GamePausedFun   () { }
    public void LevelStartedFun ()
    {
        //levelClearedPanel.SetActive(false);
        if (levelClearedPanel.activeSelf)
        {
            levelClearedPanel.GetComponent<UiTransitionCustom>().TransitionRightOrLeftToCenter(0, 2000, 0, 3, null, true);
        }
        
        UIUpdate();
    }
    public void LevelEndedFun     ()
    {
        ReferenceMaster.instance.uiDummy.UIUpdate();
        ReferenceMaster.instance.uiDummy.LevelClearedUIAppear(LevelClearedUiStateEnum.levelEndCards);
    }
    public void GameOverFun      ()
    {
        gameOverPanel.SetActive(true);
        //levelClearedPanel.GetComponent<UiTransitionCustom>().TransitionRightOrLeftToCenter(0, 2000, 0, 3, null, true);
    }

    //someCustomFuntions
    public void LevelEndCardClickUIFun()
    {
        ReferenceMaster.instance.uiDummy.UIUpdate();
        ReferenceMaster.instance.uiDummy.LevelClearedUIAppear(LevelClearedUiStateEnum.levelEndPanel);
    }

    

}
