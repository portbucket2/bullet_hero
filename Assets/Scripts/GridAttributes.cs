﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridAttributes : MonoBehaviour
{
    public LayerMask gridLayer;
    public GridAttributes frontGrid;
    public GridAttributes rightGrid;
    public GridAttributes backGrid;
    public GridAttributes leftGrid;
    public GridAttributes[] omniGrisArray;
    [SerializeField]
    bool targetted;
    [SerializeField]
    bool occupied;
    [SerializeField]
    bool targettedInAir;
    [SerializeField]
    bool occupiedInAir;
    [SerializeField]
    bool blocked;
    public int gridIndex;
    public int gridRowIndex;
    public int gridColumnIndex;

    // Start is called before the first frame update
    void Start()
    {
        GetComponentInChildren<MeshRenderer>().enabled = false;
        omniGrisArray = new GridAttributes[4];
        omniGrisArray[0] = frontGrid;
        omniGrisArray[1] = rightGrid;
        omniGrisArray[2] = backGrid;
        omniGrisArray[3] = leftGrid;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            
        }
    }
    public void GridAssignNeighbourGrids()
    {
        frontGrid = Raycast(transform.forward);
        backGrid = Raycast(-transform.forward);
        rightGrid = Raycast(transform.right);
        leftGrid = Raycast(-transform.right);
    }
    public GridAttributes Raycast(Vector3 tr)
    {
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, tr, out hit, 1f, gridLayer))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            Debug.Log("Did Hit");
            if (hit.transform.gameObject.GetComponent<GridAttributes>())
            {
                return hit.transform.gameObject.GetComponent<GridAttributes>();
            }
            else
            {
                return null;
            }
            

        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            Debug.Log("Did not Hit");
            return null;
        }
    }

    public void GridOccupy()
    {
        occupied = true;
        GridTargetRemove();
    }
    public void GridMakeEmpty()
    {
        //if (BlockedOrNot())
        //{
        //    return;
        //}
        occupied = false;
    }
    public bool OccupiedOrNot()
    {
        return occupied;
    }
    public void GridTarget()
    {
        targetted = true;
    }
    public void GridTargetRemove()
    {
        targetted = false;
    }
    public bool TargettedOrNot()
    {
        return targetted;
    }
    public void GridBlock()
    {
        blocked = true;
    }
    public void GridTBlockRemove()
    {
        blocked = false;
    }
    public bool BlockedOrNot()
    {
        return blocked;
    }
    public void GridOccupyAir()
    {
        occupiedInAir = true;
        
    }
    public void GridMakeAirEmpty()
    {

        occupiedInAir = false;
    }
    public bool OccupiedInAirOrNot()
    {
        return occupiedInAir;
    }
    public void GridAirTarget()
    {
        targettedInAir = true;
    }
    public void GridAirTargetRemove()
    {
        targettedInAir = false;
    }
    public bool TargettedInAirOrNot()
    {
        return targettedInAir;
    }
    public GridAttributes GridSerachEmptyOne(GridAttributes gridFocused )
    {
        //Debug.Log("Targettt " + gridIndex);
        GridAttributes gridResult = null;
        if (!occupied)
        {
            gridResult= gridFocused;
        }
        GridAttributes gridR = rightGrid;
        GridAttributes gridL = leftGrid;
        GridAttributes gridFr = frontGrid;

        while ((gridR != null || gridL != null) && gridResult  == null)
        {
            if (gridR != null && gridResult == null)
            {
                if (gridR.occupied )
                {
                    if (gridR.rightGrid != null)
                    {
                        gridR = gridR.rightGrid;
                    }
                    else
                    {
                        gridR = null;
                    }
                        
                }
                else
                {
                    gridResult = gridR;
                }

            }
            if (gridL != null && gridResult == null)
            {
                if (gridL.occupied )
                {
                    if ( gridL.leftGrid != null)
                    {
                        gridL = gridL.leftGrid;
                    }
                    else
                    {
                        gridL = null;
                    }
                    
                }
                else
                {
                    gridResult = gridL;
                }

            }
        }
        if (gridFr != null && gridResult == null)
        {
            gridResult = gridFr.GridSerachEmptyOne(gridFr);
        }

        if (gridResult != null)
        {
            //Debug.Log(gridResult.gridIndex);
        }
        else
        {
            //Debug.Log(gridResult);
        }
        
        return gridResult;
    }
}
