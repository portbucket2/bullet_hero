﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLmgBehavior : MonoBehaviour
{
    public float movingDuration;
    public float shootingDuration;
    public float burstDuration;
    public float burstInterval;
    EnemyPathFinding enemyPathFinding;
    EnemyShooting enemyShooting;

    // Start is called before the first frame update
    void Start()
    {
        enemyPathFinding = GetComponent<EnemyPathFinding>();
        enemyShooting = GetComponent<EnemyShooting>();
        enemyPathFinding.initialAction += CheckLmgAndInitialize;


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CheckLmgAndInitialize()
    {
        if (enemyPathFinding.lmg)
        {
            StartCoroutine(LmgMovingAndFiringCoroutine());
        }
    }
    public void LmgPauseMoving()
    {
        enemyPathFinding.PauseMoving();
        StartCoroutine(LmgFiringCoroutine());

        enemyPathFinding.enemyAnimatorController.AimUpEnemyAnim();
    }
    public void LmgResumeMoving()
    {
        //StopCoroutine(LmgFiringCoroutine());
        //enemyShooting.StopFire();
        enemyPathFinding.ResumeMoving();

        enemyPathFinding.enemyAnimatorController.AimDownEnemyAnim();
    }
    public IEnumerator LmgMovingAndFiringCoroutine()
    {
        if (enemyPathFinding.DestinedOrNotCompare())
        {
            //StartCoroutine(LmgFiringCoroutine());
            //yield return new WaitForSeconds(shootingDuration);
            //StartCoroutine(LmgMovingAndFiringCoroutine());
        }
        else
        {
            LmgResumeMoving();
            yield return new WaitForSeconds(movingDuration);
            LmgPauseMoving();
            yield return new WaitForSeconds(shootingDuration);
            LmgResumeMoving();
            //StartCoroutine( LmgMovingAndFiringCoroutine());
            LmgStopFiringLoop();
        }
    }
    public void LmgStopFiringLoop()
    {
        enemyShooting.StopFire();
        StopAllCoroutines();
        if (enemyPathFinding.DestinedOrNotCompare())
        {
            StartCoroutine(LmgFiringCoroutine());
        }
        else
        {
            StartCoroutine(LmgMovingAndFiringCoroutine());
        }
            
    }
    IEnumerator LmgFiringCoroutine()
    {
        
        enemyShooting.StartFire();
        yield return new WaitForSeconds(burstDuration);
        enemyShooting.StopFire();
        yield return new WaitForSeconds(burstInterval);
        StartCoroutine(LmgFiringCoroutine());
    
    
    }


}
