﻿public enum EnemyTypeBasedOnCovering
{
    moving, covering
}
public enum EnemyOrientation
{
      front, right, back,left
}
public enum GameStates
{
    MenuIdle, GameStarted,  GamePaused, LevelStarted, LevelEnded ,  GameOver
}
public enum UiStates
{
    Idle, Game , LevelEndCards, LevelEndPanel, GameOver
}
public enum LevelEndCardTypes
{
    AssaultRifle               ,
    RPG                        ,
    ReloadtimeDecrease    ,
    FireRateIncrease              ,
    ShieldIncrease             ,
    HealthIncrease             ,
    HealFull                  ,
    HealHalf                 ,
    DamageIncrease ,
   
}

public enum EnemyType
{
    Melee, Assault , RPG , LMG, Humvee , Tripod , Drone , BossSwat , BossTank
}

public enum PlayerWeaponType
{
    Pistol, AssaultRifle, Rpg 
}


