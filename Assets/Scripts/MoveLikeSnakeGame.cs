﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLikeSnakeGame : MonoBehaviour
{
    public GridAttributes currentGridBelow;
    public GridAttributes justLeftGridBelow;
    public float walkSpeed;
    public bool walk;
    public bool destined;
    public GridAttributes target;
    public GridAttributes nextGrid;
    public GameObject faceObj;
    public List< EnemyOrientation> priorityList;
    public Vector3 localPosMapped;
    public EnemyOrientation currentOri;
    public int currentOriIndex;
    public bool reversed;
    
    //public GameObject raycastTarget;
    //public GameObject hitObj;
    //public LayerMask rayCastLayer;
    // Start is called before the first frame update
    void Start()
    {
        //LocalPosMap();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            LocalPosMap();
            CurrentGridBelowDetermineAndOccupy();
            nextGrid = NextGridDetermine();
            WalkStartFuntion();
        }

        if (nextGrid)
        {
            if (walk)
            {
                transform.position = Vector3.MoveTowards(transform.position, nextGrid.transform.position, Time.deltaTime * 20 * walkSpeed * 0.02f);
                if (transform.position == nextGrid.transform.position)
                {
                    if (LocalMappingNeededOrNot())
                    {
                        LocalPosMap();
                    }
                    WalkEndFuntion();
                    if(nextGrid != null)
                    {
                        WalkStartFuntion();
                    }
                    
                }
            }
        }
    }
    public void PriorityListReverse()
    {
        EnemyOrientation enemyOri = priorityList[1];
        priorityList[1] = priorityList[2];
        priorityList[2] = enemyOri;

        if(currentOriIndex == 1)
        {
            currentOriIndex = 2;
            currentOri = priorityList[2];
        }
        else if (currentOriIndex == 2)
        {
            currentOriIndex = 1;
            currentOri = priorityList[1];
        }

    }
    public bool LocalMappingNeededOrNot()
    {
        bool result = false;
        if(target!= null)
        {
            Vector3 gap = transform.position - target.transform.position;
            if(Mathf.Abs(gap.x)< 0.1f || Mathf.Abs(gap.z) < 0.1f)
            {
                result = true;
            }
        }
        return result;
    }
    public void WalkStartFuntion()
    {
        walk = true;
        if(currentGridBelow != null)
        {
            currentGridBelow.GridMakeEmpty();
        }
        
    }
    public void WalkEndFuntion()
    {
        walk = false;

        CurrentGridBelowDetermineAndOccupy();

        if(currentGridBelow == target)
        {
            DestinationGrab();
            return;
        }

        nextGrid = NextGridDetermine();

        if(nextGrid == justLeftGridBelow)
        {
            PriorityListReverse();
            Debug.Log("Reversed");


            //reversed = true;
        }
    }
    public void DestinationGrab()
    {
        destined = true;
        nextGrid = null;
        target = null;
    }
    public void DestinationLeave()
    {
        destined = false;
    }
    public void LocalPosMap()
    {
        localPosMapped = faceObj.transform.InverseTransformPoint(target.transform.position);
        if(Mathf.Abs(localPosMapped.x ) > Mathf.Abs(localPosMapped.z))
        {
            
            if (localPosMapped.z > 0)
            {
                priorityList[1] = EnemyOrientation.front;
                priorityList[2] = EnemyOrientation.back;
            }
            else if (localPosMapped.z == 0)
            {
                
                for (int i = 0; i < priorityList.Count; i++)
                {
                    if (priorityList[i] == EnemyOrientation.front)
                    {
                        priorityList[1] = priorityList[i];
                        priorityList[2] = EnemyOrientation.back;
                        break;
                    }
                    else if (priorityList[i] == EnemyOrientation.back)
                    {
                        priorityList[1] = priorityList[i];
                        priorityList[2] = EnemyOrientation.front;
                        break;
                    }
                }
            }
            else
            {
                priorityList[1] = EnemyOrientation.back;
                priorityList[2] = EnemyOrientation.front;
            }
            if (localPosMapped.x > 0)
            {
                priorityList[0] = EnemyOrientation.right;
                priorityList[3] = EnemyOrientation.left;
            }

            else
            {
                priorityList[0] = EnemyOrientation.left;
                priorityList[3] = EnemyOrientation.right;
            }
        }
        else
        {
            
            if (localPosMapped.x > 0)
            {
                priorityList[1] = EnemyOrientation.right;
                priorityList[2] = EnemyOrientation.left;
            }
            else if (localPosMapped.x == 0)
            {
                EnemyOrientation er = EnemyOrientation.right;
                for (int i = 0; i < priorityList.Count; i++)
                {
                    if(priorityList[i] == EnemyOrientation.right)
                    {
                        priorityList[1] = priorityList[i];
                        priorityList[2] = EnemyOrientation.left;
                        break;
                    }
                    else if(priorityList[i] == EnemyOrientation.left)
                    {
                        priorityList[1] = priorityList[i];
                        priorityList[2] = EnemyOrientation.right;
                        break;
                    }
                }
            }
            else
            {
                priorityList[1] = EnemyOrientation.left;
                priorityList[2] = EnemyOrientation.right;
            }
            if (localPosMapped.z > 0)
            {
                priorityList[0] = EnemyOrientation.front;
                priorityList[3] = EnemyOrientation.back;
            }
            else
            {
                priorityList[0] = EnemyOrientation.back;
                priorityList[3] = EnemyOrientation.front;
            }
        }
        //int i = (int)priorityList[1];
        //priorityList[2] = (EnemyOrientation)i;
        
    }
    public void CurrentGridBelowDetermineAndOccupy()
    {
        if (currentGridBelow)
        {
            justLeftGridBelow = currentGridBelow;
        }
        currentGridBelow = ReferenceMaster.instance.gridSystem.GetMyGridBelow(transform.position);
        if (currentGridBelow != null)
        {
            currentGridBelow.GridOccupy();
        }
    }
    public GridAttributes NextGridDetermine()
    {
        GridAttributes gridNext = null;
        if(currentGridBelow == null)
        {
            return null;
        }
        //gridNext = currentGridBelow.omniGrisArray[(int)priorityList[0]];
        for (int i = 0; i < 4; i++)
        {
            gridNext = currentGridBelow.omniGrisArray[(int)priorityList[i]];
            if(gridNext != null)
            {
                if (!gridNext.OccupiedOrNot())
                {
                    currentOri = priorityList[i];
                    if(currentOriIndex != i)
                    {
                        LocalPosMap();
                    }
                    currentOriIndex = i;
                    break;
                }
                
            }
        }
        return gridNext;
    }
}
