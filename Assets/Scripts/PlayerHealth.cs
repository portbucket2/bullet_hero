﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public int playerMaxHealthValue;
    int playerHealthValue;
    public Slider playerHealthSlider;
    public Text playerHealthValueText;
    public Text playerHealthValueTextTest;
    public bool autoRefillHealth;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void DamagePlayer(int damage)
    {
        if (ReferenceMaster.instance.playerShieldFuntion.ShieldedOrNot())
        {
            ReferenceMaster.instance.playerShieldFuntion.ShieldDamageBy(damage);
            return;
        }
        playerHealthValue -= damage;
        if (playerHealthValue <= 0)
        {
            playerHealthValue = 0;
            if (autoRefillHealth)
            {
                RefillPlayerHealth();
            }
            else
            {
                ReferenceMaster.instance.gameManagementMain.GameOver();
            }
            
            //RefillPlayerHealth();
            //Destroy(this.gameObject);
        }

        HealthSliderUpdate();
    }
    public void HealthSliderUpdate()
    {
        playerHealthSlider.value = (float)playerHealthValue / playerMaxHealthValue;
        playerHealthValueText.text = (int)playerHealthValue + " / " + playerMaxHealthValue;
    }
    public void RefillPlayerHealth()
    {
        playerHealthValue = playerMaxHealthValue;
        HealthSliderUpdate();
    }

    public void SetPlayerMaxHealth( int h)
    {
        playerMaxHealthValue = h;
    }
    public void SetPlayeinitialHealth(int h)
    {
        playerMaxHealthValue = h;
        playerHealthValue = playerMaxHealthValue;

        HealthSliderUpdate();
    }
    /// <summary>
    /// ///////////////////////////////////TestPurposeStart
    /// </summary>
    public void PlayerHealthIncrease()
    {
        playerMaxHealthValue += 25;
        playerHealthValueTextTest.text = ""+playerMaxHealthValue;
    
        playerHealthValue = playerMaxHealthValue;
        playerHealthValueText.text = (int)playerHealthValue + " / " + playerMaxHealthValue;
    }
    public void PlayerHealthDecrease()
    {
        
        playerMaxHealthValue -= 25;
        if(playerMaxHealthValue < 0)
        {
            playerMaxHealthValue = 0;
        }
        playerHealthValueTextTest.text = "" + playerMaxHealthValue;
        playerHealthValue = playerMaxHealthValue;
        playerHealthValueText.text = (int)playerHealthValue + " / " + playerMaxHealthValue;
    }
    /// ///////////////////////////////////TestPurposeEnd
    public void PlayerHealthIncreaseBy(float h)
    {
        playerHealthValue += (int)h;
        if(playerHealthValue > playerMaxHealthValue)
        {
            playerHealthValue = playerMaxHealthValue;
        }
        HealthSliderUpdate();
    }

    public void PlayerHealthFullHeal()
    {
        playerHealthValue = playerMaxHealthValue;
        HealthSliderUpdate();
    }
    public void PlayerHealthHalfHeal()
    {
        playerHealthValue += (int)(playerMaxHealthValue/2);
        if (playerHealthValue > playerMaxHealthValue)
        {
            playerHealthValue = playerMaxHealthValue;
        }
        HealthSliderUpdate();
    }
}
