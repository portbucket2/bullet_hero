﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoverPointAttributes : MonoBehaviour
{
    public ObstacleWall obstacleWallMine;
    public int coverIndex;
    [SerializeField]
    bool occupied;
    public GameObject OccupiedBy;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OccupyCover()
    {
        occupied = true;
    }
    public void EmptyCover()
    {
        occupied = false;
    }
    public bool OccupiedOrNot()
    {
        return occupied;
    }
}
