﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesManager : MonoBehaviour
{
    public List<ObstacleWall> ObstacleWalls;
    public List<ObstacleWall> ObstacleWallsSorted;
    // Start is called before the first frame update
    void Start()
    {
        //InitialTasks();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetNearestCoverPoint(Vector3 pos)
    {

    }
    public void SortObstacles()
    {
        List<ObstacleWall> ObsWallsList = new List<ObstacleWall>(ObstacleWalls);
        ObstacleWallsSorted.Clear();
        //ObstacleWallsSorted = ObsWallsList;
        while (ObsWallsList.Count > 0)
        {
            int index = 0;
            float dis = 0;

            for (int i = 0; i < ObsWallsList.Count; i++)
            {
                float d = Vector3.Distance(ObsWallsList[i].transform.position, ReferenceMaster.instance.playerWeaponControl.transform.position);
                if(d>= dis)
                {
                    dis = d;
                    index = i;
                }
                
            }
            ObstacleWallsSorted.Add(ObsWallsList[index]);
            ObstacleWallsSorted[ObstacleWallsSorted.Count - 1].obstacleDistanceIndex = ObstacleWallsSorted.Count - 1;
            ObsWallsList.RemoveAt(index);
        }
    }
    public void InitialTasks(List<ObstacleWall> ObstacleWallsList)
    {
        AssignObsWallsList(ObstacleWallsList);
        SortObstacles();
        for (int i = 0; i < ObstacleWallsSorted.Count; i++)
        {
            ObstacleWallsSorted[i].InitialWallOperations();
        }
    }
    public void AssignObsWallsList(List<ObstacleWall> ObstacleWallsList)
    {
        //ObstacleWalls.Clear();
        ObstacleWalls = ObstacleWallsList;
    }
    public ObstacleWall NextCoverObsFind(ObstacleWall obstacleWall)
    {
        CoverPointAttributes coverPoint = null;
        ObstacleWall nextObsCover = null;
        float dis = Vector3.Distance( obstacleWall.transform.position , ReferenceMaster.instance.playerWeaponControl.transform.position ) ;
        int currObsIndex = obstacleWall.obstacleDistanceIndex;
        for (int i = currObsIndex+1; i < ObstacleWallsSorted.Count; i++)
        {
            float d = Vector3.Distance(ObstacleWallsSorted[i].transform.position, ReferenceMaster.instance.playerWeaponControl.transform.position);
            if(d < dis)
            {
                if(ObstacleWallsSorted[i].GetAnEmptyCoverPoint() != null)
                {
                    nextObsCover = ObstacleWallsSorted[i];
                    coverPoint = ObstacleWallsSorted[i].GetAnEmptyCoverPoint();
                    break;
                }
                
            }
        }

        return nextObsCover;
    }
    public ObstacleWall InitialCoverObsFind(Vector3 pos)
    {
        
        ObstacleWall nextObsCover = null;
        float dis = Mathf.Infinity;
        nextObsCover = ObstacleWallsSorted[0];
        //int currObsIndex = obstacleWall.obstacleDistanceIndex;
        for (int i = 0; i < ObstacleWallsSorted.Count; i++)
        {
            float d = Vector3.Distance(ObstacleWallsSorted[i].transform.position, pos);
            if (d < dis)
            {
                if (ObstacleWallsSorted[i].GetAnEmptyCoverPoint() != null)
                {
                    dis = d;
                    nextObsCover = ObstacleWallsSorted[i];
                    
                    //break;
                }

            }
        }

        return nextObsCover;
    }
}
