﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSystem : MonoBehaviour
{
    public GameObject gridObj;
    public GameObject gridsHolder;
    public float gridBlockSize;
    public int gridRows;
    public int gridCollumns;
    public List<GameObject> gridsPresent;
    public int frontLineRow;
    public GridAttributes leftEntryGrid;
    public GridAttributes rightEntryGrid;
    //public static GridSystem instance;


    // Start is called before the first frame update
    void Start()
    {
        //MakeGround();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            gridsPresent[3].GetComponent<GridAttributes>().GridSerachEmptyOne(gridsPresent[3].GetComponent<GridAttributes>());
        }
        

    }

    public void MakeGround()
    {
        Vector3 initialPos = new Vector3(gridBlockSize * 0.5f, 0, gridBlockSize * 0.5f) ;
        for (int i = 0; i < gridRows; i++)
        {
            for (int j = 0; j < gridCollumns; j++)
            {
                GameObject go = Instantiate(gridObj, transform.position, transform.rotation);
                go.transform.SetParent(gridsHolder. transform);
                go.transform.localPosition = initialPos + new Vector3(j * gridBlockSize, 0, i * gridBlockSize);
                gridsPresent.Add(go);
                go.GetComponent<GridAttributes>().gridIndex = gridsPresent.Count - 1;
                go.GetComponent<GridAttributes>().gridRowIndex = i;
                go.GetComponent<GridAttributes>().gridColumnIndex = j;

            }
        }
        gridsHolder.transform.localPosition = -new Vector3(gridCollumns * gridBlockSize * 0.5f, 0, 0);
    }

    public GridAttributes GetMyGrid(Vector3 worldPos, bool frontLine = false)
    {
        GridAttributes gridAttr = null;
        int index = 0;
        Vector3 localPos = gridsHolder.transform.InverseTransformPoint(worldPos);
        
        int row = (int)(localPos.z / gridBlockSize);
        if (frontLine)
        {
            row = 0;
        }
        int column  = (int)(localPos.x / gridBlockSize);
        index = ((row  )* gridCollumns )+ (column+1) - 1;
        
        
        if(index < 0)
        {
            gridAttr = null;
        }
        else
        {
            gridAttr =  gridsPresent[index].GetComponent<GridAttributes>().GridSerachEmptyOne(gridsPresent[index].GetComponent<GridAttributes>());
        }
        return gridAttr;
    }
    public GridAttributes GetMyGridBelow(Vector3 worldPos)
    {
        GridAttributes gridAttr = null;
        int index = 0;
        Vector3 localPos = gridsHolder.transform.InverseTransformPoint(worldPos);

        int row = (int)(localPos.z / gridBlockSize);
        if (row >= gridRows) row = gridRows-1;
        int column = (int)(localPos.x / gridBlockSize);
        if (column >= gridCollumns) column = gridCollumns - 1;
        index = ((row) * gridCollumns) + (column + 1) - 1;
        //Debug.Log("row " + row+ " column "+ column + " index "+ index);

        if (index < 0 || row < 0 || column<0 || index > (gridsPresent.Count -1))
        {
            gridAttr = null;
        }
        else
        {
            gridAttr = gridsPresent[index].GetComponent<GridAttributes>();
        }
        return gridAttr;
    }
    //public int GetMyGridAtFrontline(Vector3 worldPos)
    //{
    //    int index = 0;
    //    Vector3 localPos = gridsHolder.transform.InverseTransformPoint(worldPos);
    //    int row = 0;
    //    int column = (int)(localPos.x / gridBlockSize);
    //    index = ((row) * gridCollumns) + (column + 1) - 1;
    //    
    //
    //
    //    return index;
    //}
    public void GridUnitsAssignNeighbourGrids()
    {
        for (int i = 0; i < gridsPresent.Count; i++)
        {
            gridsPresent[i].GetComponent<GridAttributes>().GridAssignNeighbourGrids();
        }
    }
    public int FrontlineDetermine()
    {
        int firstUnOccupied = 0;
        for (int i = 0; i < gridsPresent.Count; i++)
        {
            if (!gridsPresent[i].GetComponent<GridAttributes>().OccupiedOrNot())
            {
                firstUnOccupied = i;
                Debug.Log("UNOCCUPIED " + i);
                break;
            }
        }
        frontLineRow = (int)((float)firstUnOccupied / gridCollumns);
        return frontLineRow;
    }
    public void RenewAllGrids()
    {
        for (int i = 0; i < gridsPresent.Count; i++)
        {
            gridsPresent[i].GetComponent<GridAttributes>().GridMakeEmpty();
            gridsPresent[i].GetComponent<GridAttributes>().GridTargetRemove();
            gridsPresent[i].GetComponent<GridAttributes>().GridTBlockRemove();

        }
    }
    public void IdentifyEntryGrids()
    {
        leftEntryGrid = gridsPresent[(gridsPresent.Count - 1) - gridCollumns + 1].GetComponent<GridAttributes>();
        rightEntryGrid = gridsPresent[gridsPresent.Count - 1].GetComponent<GridAttributes>();
    }
}
