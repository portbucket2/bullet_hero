﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public int enemyMaxHealthValue;
    int enemyHealthValue;
    public Slider enemyHealthSlider;
    public int enemySpawnIndex;
    // Start is called before the first frame update
    void Start()
    {
        //enemyHealthValue = enemyMaxHealthValue;
        //StartCoroutine(TestCoroutine(.2f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DamageEnemy(int damage)
    {
        enemyHealthValue -= damage;
        if(enemyHealthValue <= 0)
        {
            enemyHealthValue = 0;
            this.GetComponent<EnemyPathFinding>(). EnemyDeathNote();
        }

        enemyHealthSlider.value = (float)enemyHealthValue / enemyMaxHealthValue;
    }
    IEnumerator TestCoroutine(float t)
    {
        yield return new WaitForSeconds(1f);
        Debug.Log("I AM HEALTHY");
        StartCoroutine( TestCoroutine(t));



    }
    public void EnemyHealthValueAssign()
    {
        EnemyTypeBasedOnCovering enemyTypee = EnemyTypeBasedOnCovering.moving;
        EnemyPathFinding enemyPathFinding = GetComponent<EnemyPathFinding>();

        switch (enemyTypee)
        {
            case EnemyTypeBasedOnCovering.moving:
                {
                    if (enemyPathFinding.rpg)
                    {
                        
                    }
                    else if (enemyPathFinding.lmg)
                    {
                        
                    }
                    else if (enemyPathFinding.humvee)
                    {
                        
                    }
                    else if (enemyPathFinding.tripod)
                    {
                        
                    }
                    else if (enemyPathFinding.drone)
                    {
                        
                    }
                    else if (enemyPathFinding.bossSwat)
                    {

                    }
                    else if (enemyPathFinding.bossTank)
                    {

                    }
                    else
                    {
                       
                    }
                    break;
                }
            case EnemyTypeBasedOnCovering.covering:
                {
                    
                    break;
                }

        }
    }
    public void SetEnemyHealth(int h)
    {
        enemyMaxHealthValue = h;
        enemyHealthValue = enemyMaxHealthValue;
    }
}
