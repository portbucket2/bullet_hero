﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelProgressionWallsInfo : MonoBehaviour
{
    //public List<ObstacleWall> level1Walls;
    //public List<ObstacleWall> level2Walls;
    //public List<ObstacleWall> level3Walls;
    //public List<ObstacleWall> level4Walls;

    public List<ObstacleWall> LevelWallsExtracted;
    public List<levelWalls> levelWallsList;
    [System.Serializable]
    public struct levelWalls
    {
        public string lvlInfo;
        public List<ObstacleWall> walls;
    }
    public GameObject[] levelVisuals;

    // Start is called before the first frame update
    void Start()
    {
        //LevelWallsExtracted = levelVisuals[4].GetComponentsInChildren<ObstacleWall>();
        //foreach(ObstacleWall obs in levelVisuals[4].GetComponentsInChildren<ObstacleWall>())
        //{
        //    LevelWallsExtracted.Add(obs);
        //}
        ExtractWallsOFAllLevels();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ExtractWallsOFCurrentLevel(int levelIndex)
    {
        levelWallsList[levelIndex].walls.Clear();
        foreach (ObstacleWall obs in levelVisuals[levelIndex].GetComponentsInChildren<ObstacleWall>())
        {
            //LevelWallsExtracted.Add(obs);
            levelWallsList[levelIndex].walls.Add(obs);
        }
    }
    public void ExtractWallsOFAllLevels()
    {
        for (int i = 0; i < levelVisuals.Length; i++)
        {
            ExtractWallsOFCurrentLevel(i);
        }
    }
    public void LevelObstaclesEnable(int levelIndex)
    {
        for (int i = 0; i < levelVisuals.Length; i++)
        {
            levelVisuals[i].SetActive(false);

        }
        levelVisuals[levelIndex].SetActive(true);
        ReferenceMaster.instance.obstacleManager.InitialTasks(levelWallsList[levelIndex].walls);
        //switch (levelIndex)
        //{
        //    case 0:
        //        {
        //            ReferenceMaster.instance.obstacleManager.InitialTasks(levelWallsList[0].walls);
        //            break;
        //        }
        //    case 1:
        //        {
        //            ReferenceMaster.instance.obstacleManager.InitialTasks(levelWallsList[1].walls);
        //            break;
        //        }
        //    case 2:
        //        {
        //            ReferenceMaster.instance.obstacleManager.InitialTasks(levelWallsList[2].walls);
        //            break;
        //        }
        //    case 3:
        //        {
        //            ReferenceMaster.instance.obstacleManager.InitialTasks(levelWallsList[3].walls);
        //            break;
        //        }
        //
        //}
        
    }
}
