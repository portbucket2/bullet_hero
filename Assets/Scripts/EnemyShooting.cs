﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    public GameObject gunMuzzle;
    public GameObject bulletEnemy;
    [SerializeField]
    float fireInterval;
    public GameObject gunEnemy;
    public bool shootingOn;
    public EnemyPathFinding enemyPathFinding;
    EnemyLmgBehavior enemyLmgBehavior;
    [SerializeField]
    int bulletDamage;
    [SerializeField]
    float bulletSpeed;
    public float initialDelay;
    //public EnemyType enemyType;
    // Start is called before the first frame update
    void Start()
    {
        //StartFire();
        enemyPathFinding = GetComponent<EnemyPathFinding>();
        enemyLmgBehavior = GetComponent<EnemyLmgBehavior>();
        //fireInterval = FiringIntervalDetermine(enemyPathFinding);
        //enemyPathFinding.initialAction += ShootingFuntionMoving;
        enemyPathFinding.lmgDestinedAction += ShootingFuntionLMG;
        enemyPathFinding.movingEndedAction += ShootingFuntionAtMoveEnd;

        if (enemyPathFinding.lmg)
        {
            //StartCoroutine(LmgFiringCoroutine());
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        //PointGunAtPlayer();
        
        
        
    }
    public void Shoot()
    {
        PointGunAtPlayer();
        //GameObject bullet =  Instantiate(bulletEnemy, gunMuzzle.transform.position, gunMuzzle.transform.rotation);
        GameObject bullet = PoolingSystemShawon.instance.bulletPoolEnemy.InstantiateFromPool(gunMuzzle.transform.position, gunMuzzle.transform.rotation);
        BulletEnemy bulletEnemyy = bullet.GetComponent<BulletEnemy>();
        bulletEnemyy.damage = bulletDamage;
        bulletEnemyy.velocity = bulletSpeed;

        enemyPathFinding.enemyAnimatorController.AttackEnemyAnim();
    }
    public void StartFire()
    {
        StartCoroutine(StartFireCoroutine(initialDelay));
        shootingOn = true;
        //Debug.Log("FIRING STARTED");

    }
    public void StopFire()
    {
        shootingOn = false;
        
        StopAllCoroutines();
        //Debug.Log("FIRING STopped");
    }
    public IEnumerator StartFireCoroutine(float initialDelay)
    {
        yield return new WaitForSeconds(initialDelay);
        StartCoroutine(FireCoroutine(fireInterval));
    }
    public IEnumerator FireCoroutine(float interval)
    {
        
        Shoot();
        yield return new WaitForSeconds(interval);
        StartCoroutine(FireCoroutine(interval));
    }
    public void PointGunAtPlayer()
    {
        gunEnemy.transform.rotation = Quaternion.LookRotation(-gunMuzzle. transform.position + ReferenceMaster.instance.playerWeaponControl.transform.position, Vector3.up);
    }
    public void SetBulletDamage(int dmg)
    {
        bulletDamage = dmg;
    }
    public void SetBulletSpeed(float bulletSpeedd)
    {
        bulletSpeed = bulletSpeedd;
    }
    public void SetInitialDealy(float initialDealyy)
    {
        initialDelay = initialDealyy;
    }
    public void SetFiringInvervalFromAttackPerSec(float attackPerSec)
    {
        if(attackPerSec <=  0)
        {
            return;
        }
        float interval = 1 / (float)attackPerSec;
        fireInterval = interval;
    }
    //public int BulletDamageDetermine(EnemyTypeBasedOnCovering enemyTypee)
    //{
    //    int damage = 0;
    //    switch (enemyTypee)
    //    {
    //        case EnemyTypeBasedOnCovering.moving:
    //            {
    //                if (enemyPathFinding.rpg)
    //                {
    //                    damage = 10;
    //                }
    //                else if (enemyPathFinding.lmg)
    //                {
    //                    damage = 10;
    //                }
    //                else if (enemyPathFinding.humvee)
    //                {
    //                    damage = 10;
    //                }
    //                else if (enemyPathFinding.tripod)
    //                {
    //                    damage = 10;
    //                }
    //                else if (enemyPathFinding.drone)
    //                {
    //                    damage = 10;
    //                }
    //                else if (enemyPathFinding.bossSwat)
    //                {
    //                    damage = 10;
    //                }
    //                else if (enemyPathFinding.bossTank)
    //                {
    //                    damage = 10;
    //                }
    //                else
    //                {
    //                    damage = 10;
    //                }
    //                break;
    //            }
    //        case EnemyTypeBasedOnCovering.covering:
    //            {
    //                damage = 5;
    //                break;
    //            }
    //
    //    }
    //    return damage;
    //}
    //public int BulletSpeedDetermine(EnemyTypeBasedOnCovering enemyTypee)
    //{
    //    int bulletSpeed = 0;
    //    switch (enemyTypee)
    //    {
    //        case EnemyTypeBasedOnCovering.moving:
    //            {
    //                bulletSpeed = 5;
    //                if (enemyPathFinding.rpg)
    //                {
    //                    bulletSpeed = 5;
    //                }
    //                else if (enemyPathFinding.lmg)
    //                {
    //                    bulletSpeed = 5;
    //                }
    //                else if (enemyPathFinding.humvee)
    //                {
    //                    bulletSpeed = 5;
    //                }
    //                else if (enemyPathFinding.tripod)
    //                {
    //                    bulletSpeed = 5;
    //                }
    //                else if (enemyPathFinding.drone)
    //                {
    //                    bulletSpeed = 5;
    //                }
    //                else if (enemyPathFinding.bossSwat)
    //                {
    //                    bulletSpeed = 5;
    //                }
    //                else if (enemyPathFinding.bossTank)
    //                {
    //                    bulletSpeed = 5;
    //                }
    //                else
    //                {
    //                    bulletSpeed = 5;
    //                }
    //                break;
    //            }
    //        case EnemyTypeBasedOnCovering.covering:
    //            {
    //                bulletSpeed = 4;
    //                break;
    //            }
    //
    //    }
    //    return bulletSpeed;
    //}
    public void ShootingFuntionMoving()
    {
        EnemyTypeBasedOnCovering enemyTypee = enemyPathFinding.enemyType;
        switch (enemyTypee)
        {
            case EnemyTypeBasedOnCovering.moving:
                {
                    if (!enemyPathFinding.rpg && !enemyPathFinding.lmg)
                    {
                        if (!shootingOn)
                        {
                            
                            StartFire();
                        }
                    }
                    break;
                }
            case EnemyTypeBasedOnCovering.covering:
                {
                    break;
                }

        }
    }
    public void ShootingFuntionAtMoveEnd()
    {
        EnemyTypeBasedOnCovering enemyTypee = enemyPathFinding.enemyType;
        switch (enemyTypee)
        {
            case EnemyTypeBasedOnCovering.moving:
                {
                    if (enemyPathFinding.rpg || enemyPathFinding.lmg)
                    {
                        if (enemyPathFinding.lmg)
                        {
                            
                            //StartCoroutine(LmgFiringCoroutine());
                        }
                        
                    }
                    else
                    {
                        if (!shootingOn)
                        {

                            StartFire();
                        }
                    }
                    
                    break;
                }
            case EnemyTypeBasedOnCovering.covering:
                {
                    break;
                }

        }
    }
    public void ShootingFuntionLMG()
    {
        EnemyTypeBasedOnCovering enemyTypee = enemyPathFinding.enemyType;
        switch (enemyTypee)
        {
            case EnemyTypeBasedOnCovering.moving:
                {
                    if (enemyPathFinding.rpg)
                    {
                        if (!shootingOn)
                        {

                            StartFire();
                        }
                    }
                    break;
                }
            case EnemyTypeBasedOnCovering.covering:
                {
                    break;
                }

        }
    }
    public float FiringIntervalDetermine( EnemyPathFinding enemyPathFindingg)
    {
        EnemyTypeBasedOnCovering enemyTypee = enemyPathFindingg.enemyType;
        float interval = 0;
        switch (enemyTypee)
        {
            case EnemyTypeBasedOnCovering.moving:
                {
                    interval = 1f;
                    if (enemyPathFinding.rpg)
                    {
                        interval = .5f;
                    }
                    if (enemyPathFinding.lmg)
                    {
                        interval = .2f;
                    }
                    if (enemyPathFinding.humvee)
                    {
                        interval = .2f;
                    }
                    if (enemyPathFinding.drone)
                    {
                        interval = .33f;
                    }
                    else if (enemyPathFinding.bossSwat)
                    {
                        interval = .33f;
                    }
                    else if (enemyPathFinding.bossTank)
                    {
                        interval = .33f;
                    }
                    break;
                }
            case EnemyTypeBasedOnCovering.covering:
                {
                    interval = 1f;
                    break;
                }

        }

        return interval;
    }

    //public IEnumerator LmgFiringCoroutine()
    //{
    //    //StopAllCoroutines();
    //    StartFire();
    //    yield return new WaitForSeconds(enemyLmgBehavior. burstDuration);
    //    StopFire();
    //    yield return new WaitForSeconds(enemyLmgBehavior.burstInterval);
    //    StartCoroutine(LmgFiringCoroutine());
    //
    //
    //}
}
