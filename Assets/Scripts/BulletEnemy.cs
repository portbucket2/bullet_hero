﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemy : MonoBehaviour
{
    public float velocity;
    public int damage;
    public float bulletLifetime;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, 0, velocity * Time.deltaTime * 10 * 0.3f);
    }
    private void OnCollisionEnter(Collision collision)
    {
        //Destroy(this.gameObject);
        if(collision.gameObject.layer == 12)
        {
            ReferenceMaster.instance.playerHealth.DamagePlayer(damage);
            //Destroy(this.gameObject);
            DestroyBullet();
        }
    }

    public void DestroyBullet(float t = 0)
    {
        StopAllCoroutines();
        StartCoroutine(BulleyDestroyCoroutine(t));


    }
    IEnumerator BulleyDestroyCoroutine(float t)
    {
        yield return new WaitForSeconds(t);
        PoolingSystemShawon.instance.bulletPoolEnemy.DestroyBackToPool(this.gameObject);
    }
    void OnEnable()
    {
        DestroyBullet(bulletLifetime);
    }
}
