﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerShieldFuntion : MonoBehaviour
{
    public bool shielded;
    public GameObject shieldObj;
    Quaternion targetRot;
    bool transitioning;
    public float shieldHealthMax;
    public float shieldHealth;
    public Slider shieldSlider;
    bool recovering;
    //public bool deadShield;
    public float shieldRecoverySpeed;
    // Start is called before the first frame update
    void Start()
    {
        ReferenceMaster.instance.touchInputSystem.actionTouchedIn += ShieldDisable;
        
        ReferenceMaster.instance.touchInputSystem.actionTouchedOut += ShieldEnable;
        ShieldEnable();
    }

    // Update is called once per frame
    void Update()
    {
        if (transitioning)
        {
            shieldObj.transform.localRotation = Quaternion.RotateTowards(shieldObj.transform.localRotation, targetRot, Time.deltaTime * 500);
            if (shieldObj.transform.localRotation == targetRot)
            {
                transitioning = false;
            }
        }
        ShieldRecovery();


    }
    public void ShieldEnable()
    {
        if (!shielded)
        {
            targetRot = Quaternion.Euler(new Vector3(0, 0, 0));
            shielded = true;
            transitioning = true;
        }
        recovering = false;
    }
    public void ShieldDisable()
    {
        if (shielded)
        {
            targetRot = Quaternion.Euler(new Vector3(0, -90, 0));
            shielded = false;
            transitioning = true;
        }
        recovering = true;
        
    }

    public void ShieldDamageBy(int d)
    {
        shieldHealth -= d;
        if(shieldHealth < 0)
        {
            shieldHealth = 0;
            //ShieldDisable();
            
        }
        ShieldSliderUpdate();
    }
    public void ShieldSliderUpdate()
    {
        shieldSlider.value = (float)shieldHealth / (float)shieldHealthMax;
    }
    public void SetShieldInitialHealthMax(int h)
    {
        shieldHealthMax = (float)h;
        shieldHealth = shieldHealthMax;
        ShieldSliderUpdate();
    }
    public void ShieldRecovery()
    {
        if(recovering && shieldHealth < shieldHealthMax)
        {
            shieldHealth += Time.deltaTime * shieldRecoverySpeed;
            ShieldSliderUpdate();
            
        }
        
    }
    public void ShieldIncreaseBy(float s)
    {
        shieldHealth += (int)s;
        if(shieldHealth > shieldHealthMax)
        {
            shieldHealth = shieldHealthMax;
        }
        ShieldSliderUpdate();
    }
    public bool ShieldedOrNot()
    {
        bool ret = false;
        if( shieldHealth > 0)
        {
            ret = shielded;
        }
        
        return ret;
    }
}
