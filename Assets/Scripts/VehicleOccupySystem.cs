﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleOccupySystem : MonoBehaviour
{
    public GridAttributes gridOne;
    public GridAttributes gridTwo;
    public GameObject body;
    Quaternion targetRot;
    Quaternion endRot;
    public float rotSpeed;
    public bool transition;
    MoverToAnyPoint moverToAnyPoint;
    Quaternion oldRot;
    public float angularSpeed;
    //public GridAttributes targetToStand;
    // Start is called before the first frame update
    void Start()
    {
        moverToAnyPoint = GetComponent<MoverToAnyPoint>();
        moverToAnyPoint.twoGridFoundAction += GetTwoGrids;
    }

    // Update is called once per frame
    void Update()
    {
        if (gridOne)
        {
            if (transition)
            {
                targetRot = endRot;
            }
            else
            {
                targetRot = Quaternion.LookRotation(gridOne.transform.position - transform.position, Vector3.up);
            }
            oldRot = body.transform.rotation;
            body.transform.rotation = Quaternion.Lerp(body.transform.rotation, targetRot, Time.deltaTime * rotSpeed);
            //angularSpeed = MeasureRotSpeed();
            //body.transform.localPosition = new Vector3(-angularSpeed * 1.0f, 0, 0);
        }
    }

    public void GetTwoGrids()
    {
        GridAttributes gridOnee = moverToAnyPoint.target;
        GridAttributes gridTwoo = moverToAnyPoint.secondGrid;
        gridOne = gridOnee;
        gridTwo = gridTwoo;
        if(Vector3.Distance(transform.position , gridOne.transform.position) > Vector3.Distance(transform.position, gridTwo.transform.position))
        {
            gridOne = gridTwoo;
            gridTwo = gridOnee;
        }

        endRot = Quaternion.LookRotation(gridTwo.transform.position - gridOne.transform.position, Vector3.up);

    }

    public float MeasureRotSpeed()
    {
        float rotSpeed = 0;
        Vector3 oldRotDeg = Quaternion.ToEulerAngles(oldRot) * Mathf.Rad2Deg;
        Vector3 newRotDeg = Quaternion.ToEulerAngles(body.transform.rotation) * Mathf.Rad2Deg;
        
        rotSpeed = ((newRotDeg.y - oldRotDeg.y)/Time.deltaTime )* 0.005f;
        Debug.Log(rotSpeed);

        return rotSpeed;
    }
}
