﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCanvas : MonoBehaviour
{
    public Text title;
    // Start is called before the first frame update
    void Start()
    {
        EnemyTypeBasedOnCovering enemyT = GetComponent<EnemyPathFinding>().enemyType;
        string ttl = "";
        EnemyPathFinding enemyPathFinding = GetComponent<EnemyPathFinding>();

        switch (enemyT)
        {
            case EnemyTypeBasedOnCovering.moving:
                {
                    ttl = "MELEE";
                    if (enemyPathFinding.rpg)
                    {
                        ttl = "RPG";
                    }
                    if (enemyPathFinding.lmg)
                    {
                        ttl = "LMG";
                    }
                    if (enemyPathFinding.humvee)
                    {
                        ttl = "HUMVEE";
                    }
                    if (enemyPathFinding.drone)
                    {
                        ttl = "DRONE";
                    }
                    if (enemyPathFinding.tripod)
                    {
                        ttl = "TRIPOD";
                    }
                    if (enemyPathFinding.bossSwat)
                    {
                        ttl = "BOSS SWAT";
                    }
                    if (enemyPathFinding.bossTank)
                    {
                        ttl = "BOSS TANK";
                    }
                    break;
            }
            case EnemyTypeBasedOnCovering.covering:
                {
                    ttl = "ASSAULT";
                    break;
            }

        }

        title.text = "" + ttl;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
