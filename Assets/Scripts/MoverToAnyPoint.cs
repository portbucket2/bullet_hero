﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MoverToAnyPoint : MonoBehaviour
{
    public GridAttributes currentGridBelow;
    public float walkSpeed;
    public bool walk;
    public bool paused;
    public bool destined;
    public GridAttributes target;
    public GridAttributes secondGrid;
    public GameObject raycastTarget;
    public GameObject hitObj;
    public GameObject lastHitObj;
    public LayerMask rayCastLayer;
    //public bool GetTwoGrid;
    public Action twoGridFoundAction;
    public Action ActionDestinationReach;
    public Action ActionDestinationLeave;
    public Action ActionStationLeave;
    public Action ActionStationReach;


    public int movingRangeMax;
    public int movingRangeMin;
    public GameObject cube;
    public int Attempts;
    public bool redunduncy;
    public int alreadyOnExit;
    public List<GridAttributes> validGridsToMove;
    // Start is called before the first frame update
    void Start()
    {
        //CurrentGridBelowDetermineAndOccupy();
        // SearchForAValidRaycastGridTarget(currentGridBelow);



    }

    // Update is called once per frame
    void Update()
    {
       //if (Input.GetKeyDown(KeyCode.X))
       //{
       //    CurrentGridBelowDetermineAndOccupy();
       //    GridAttributes gr = SearchForAValidRaycastGridTarget(currentGridBelow);
       //    if (gr != null)
       //    {
       //        raycastTarget = gr.gameObject;
       //        InitializeMoving(raycastTarget);
       //    }
       //    
       //    
       //    //InitializeMoving(SearchForAValidRaycastGridTarget(currentGridBelow).gameObject);
       //    
       //}
       if (Input.GetKeyDown(KeyCode.Z))
       {
           CurrentGridBelowDetermineAndOccupy();
           InitializeMoving(raycastTarget);
       }

        if (target)
        {
            if (walk && !paused)
            {
                transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime * 20 * walkSpeed * 0.02f);
                if ( Vector3.Distance(transform.position , target.transform.position) < 0.05f)
                {
                    transform.position = target.transform.position;
                    WalkEndActivity();
                    TargetGrabPostActivity();
                }
            }
        }
    }

    public void InitializeMoving(GameObject rayCastTar = null)
    {
        //CurrentGridBelowDetermineAndOccupy();

        if (rayCastTar != null)
        {
            raycastTarget = rayCastTar.gameObject;
            raycastTarget.GetComponent<GridAttributes>().GridTarget();
        }
        else
        {
            return;
        }
        DetermineNextTargetThroughRaycast();
        if (target != null)
        {
            walk = true;
            WalkStartActivity();
            destined = false;
            ActionStationLeave?.Invoke();
        }
    }

    public void WalkStartActivity()
    {
        DestinationLeaveFunction();
        
        if(currentGridBelow != null)
        {
            currentGridBelow.GridMakeEmpty();
            
        } 
        
    }
    public void WalkEndActivity()
    {
        walk = false;
        target = null;
        secondGrid = null;

        ActionStationReach?.Invoke();
        if (RaycastNeededOrNot())
        {
            DetermineNextTargetThroughRaycast();
        }
        else
        {
            DestinationReachFunction();
        }

        CurrentGridBelowDetermineAndOccupy();


    }
    public void DestinationReachFunction()
    {
        destined = true;
        target = null;
        secondGrid = null;
        raycastTarget = null;
        ActionDestinationReach?.Invoke();
    }
    public void DestinationLeaveFunction()
    {
        destined = false;
        ActionDestinationLeave?.Invoke();

    }
    public void TargetGrabPostActivity()
    {
        if(target != null)
        {
            walk = true;
            //WalkStartActivity();
            ActionStationLeave?.Invoke();
          

        }

        
    }
    public bool RaycastNeededOrNot()
    {
        bool result = false;
        if (raycastTarget != null)
        {
            if(Vector3.Distance(transform.position , raycastTarget.transform.position) < 0.1f)
            {
                result = false;
                //transform.position = raycastTarget.transform.position;
            }
            else
            {
                result = true;
            }
        }
        return result;
    }
    public GridAttributes CurrentGridBelowDetermineAndOccupy()
    {
        GridAttributes gridBelow = null;
        currentGridBelow = ReferenceMaster.instance.gridSystem.GetMyGridBelow(transform.position);
        if(currentGridBelow != null)
        {
            currentGridBelow.GridOccupy();
        }
        return gridBelow;
    }
    public void DetermineNextTargetThroughRaycast()
    {
        if(raycastTarget == null)
        {
            return;
        }
        redunduncy = false;
        //alreadyOnExit = -1;
        GridAttributes resultGrid = RaycastToRaycastTarget(raycastTarget.transform.gameObject);
        if (resultGrid == null)
        {
             target = ReferenceMaster.instance.gridSystem.GetMyGridBelow(raycastTarget.transform.position);
        }
        else
        {
            target = resultGrid;
        }

        if(target != null)
        {
            //if (GetTwoGrid)
            //{
            //    secondGrid = SearchSecondGrid(target);
            //    if(secondGrid == null)
            //    {
            //        target = null;
            //    }
            //    else
            //    {
            //        twoGridFoundAction?.Invoke();
            //    }
            //}
        }
    }
    public GridAttributes RaycastToRaycastTarget(GameObject raycastTarget)
    {
        //Debug.Log("ALR Pre" + alreadyOnExit);

        RaycastHit hit;
        if(hitObj != null)
        {
            lastHitObj = hitObj;
        }
        
        hitObj = null;
        float hitDistance = 0;
        bool hitted = false;
        Vector3 dir = raycastTarget.gameObject.transform.position - transform.position;
        float rayLenth = Vector3.Distance(transform.position, raycastTarget.gameObject.transform.position);
        GridAttributes gridAttrReturn = null;
        //Debug.Log("rayTarget " + raycastTarget.gameObject.name);

        if ((Physics.Raycast(transform.position, dir, out hit, rayLenth, rayCastLayer)))
        {

            hitDistance = hit.distance;
            hitObj = hit.transform.gameObject;
            hitted = true;
            //Debug.DrawRay(transform.position, dir, Color.yellow);
            Debug.Log("Did Hit " + hit.transform.gameObject.name);
        }
        if (hitted)
        {
            if (hitObj.layer == 11)
            {
                Debug.Log("Did Hit obs" + hit.transform.gameObject.name);

                if (hitObj == lastHitObj)
                {
                    Debug.Log("SameHIT");
                    gridAttrReturn = hit.transform.gameObject.GetComponent<ObstacleWall>().GeNexttGridExit(alreadyOnExit); 
                }
                else
                {
                    alreadyOnExit = -1;
                    GridAttributes gridExitt = hit.transform.gameObject.GetComponent<ObstacleWall>().GetGridNearestExit(transform.position);
                    gridAttrReturn = gridExitt;
                }

                alreadyOnExit = hit.transform.gameObject.GetComponent<ObstacleWall>().GetExitPointIndex(gridAttrReturn);
                if(transform.position == gridAttrReturn.transform.position)
                {
                    gridAttrReturn = hit.transform.gameObject.GetComponent<ObstacleWall>().GeNexttGridExit(alreadyOnExit);
                    alreadyOnExit = hit.transform.gameObject.GetComponent<ObstacleWall>().GetExitPointIndex(gridAttrReturn);
                }

                //if (hit.transform.gameObject.GetComponent<ObstacleWall>().CheckItAnExitPointOrNot(raycastTarget))
                //{
                //    Debug.Log("GOTTT " );
                //    gridAttrReturn = hit.transform.gameObject.GetComponent<ObstacleWall>().GeNexttGridExit(raycastTarget.GetComponent<GridAttributes>());
                //}
                //else
                //{
                //    GridAttributes gridExitt = hit.transform.gameObject.GetComponent<ObstacleWall>().GetGridNearestExit(transform.position);
                //    gridAttrReturn = gridExitt;
                //    //gridAttrReturn = gridExitt.GridSerachEmptyOne(gridExitt);
                //}
                ////GridAttributes gridExitt = hit.transform.gameObject.GetComponent<ObstacleWall>().GetGridNearestExit(transform.position);
                ////gridAttrReturn = gridExitt.GridSerachEmptyOne(gridExitt);
                //if(gridAttrReturn == currentGridBelow && !redunduncy)
                //{
                //    redunduncy = true;
                //    Debug.Log("red " + gridAttrReturn);
                //    //hit.transform.gameObject.GetComponent<ObstacleWall>().GeNexttGridExit(gridAttrReturn);
                //    gridAttrReturn = hit.transform.gameObject.GetComponent<ObstacleWall>().GeNexttGridExit(gridAttrReturn);
                //    //Debug.Log("EXT " + a);
                //}
                //alreadyOnExit = hit.transform.gameObject.GetComponent<ObstacleWall>().GetExitPointIndex(gridAttrReturn);
                //Debug.Log("ALR " + alreadyOnExit);
                //if (hitObj == lastHitObj)
                //{
                //    redunduncy = true;
                //    gridAttrReturn = hit.transform.gameObject.GetComponent<ObstacleWall>().GeNexttGridExit(gridAttrReturn);
                //    //Debug.Log("EXT " + a);
                //}
            }
        }
        if(gridAttrReturn != null )
        {
            //Debug.Log("grd " + gridAttrReturn.gridIndex);
            GridAttributes gridAttrReturnAgain = RaycastToRaycastTarget(gridAttrReturn.transform.gameObject);
            Debug.Log("grd Again " + gridAttrReturn.gridIndex);
            if (gridAttrReturnAgain != null)
            {
                gridAttrReturn = gridAttrReturnAgain;
            }
        }
        if(gridAttrReturn != null)
        {
            //Debug.Log("grd return  " + gridAttrReturn.gridIndex);
        }
        

        return gridAttrReturn;
    }

    public GridAttributes SearchSecondGrid(GridAttributes grid  )
    {
        GridAttributes gridSecond = null;
        if (gridSecond == null && grid.rightGrid != null && !grid.rightGrid.OccupiedOrNot())
        {
            gridSecond = grid.rightGrid;
        }
        if (gridSecond == null && grid.frontGrid != null && !grid.frontGrid.OccupiedOrNot())
        {
            gridSecond = grid.frontGrid;
        }
        if (gridSecond == null && grid.backGrid != null && !grid.backGrid.OccupiedOrNot())
        {
            gridSecond = grid.backGrid;
        }
        if (gridSecond == null && grid.leftGrid != null && !grid.leftGrid.OccupiedOrNot())
        {
            gridSecond = grid.leftGrid;
        }


        return gridSecond ;
    }
    public GridAttributes SearchForAValidRaycastGridTarget(GridAttributes gridBelow)
    {
        Attempts = 0;
        if (gridBelow == null)
        {
            return null;
        }
        //CurrentGridBelowDetermineAndOccupy();
        GridAttributes result = null;
    
        List<int> validRows = new List<int>();
        List<int> validCollumns = new List<int>();
        List<GridAttributes> validGrids = new List<GridAttributes>();
    
        int currentRow     = gridBelow.gridRowIndex;
        int currentCollumn = gridBelow.gridColumnIndex;
    
        int minRow     =Mathf.Clamp( currentRow - (movingRangeMin -1) , 0 , (ReferenceMaster.instance.gridSystem.gridRows -1)); 
        int maxRow     = Mathf.Clamp(currentRow + (movingRangeMin - 1), 0, (ReferenceMaster.instance.gridSystem.gridRows - 1));
        int minCollumn = Mathf.Clamp(currentCollumn - movingRangeMax, 0, (ReferenceMaster.instance.gridSystem.gridCollumns - 1));
        int maxCollumn = Mathf.Clamp(currentCollumn + movingRangeMax, 0, (ReferenceMaster.instance.gridSystem.gridCollumns- 1));
    
        //Debug.Log( minRow     +" "+
        //           maxRow     + " "+
        //           minCollumn + " "+
        //           maxCollumn + " " + currentRow
        //          );
    
        for (int i = movingRangeMin; i <= movingRangeMax; i++)
        {
            int s = currentRow + i;
            if(s < ReferenceMaster.instance.gridSystem.gridRows)
            {
                validRows.Add(s);
                //Debug.Log("Val R " + s);
            }
             s = currentRow - i;
            if (s >= 0)
            {
                validRows.Add(s);
                //Debug.Log("Val R " + s);
            }
            s = currentCollumn + i;
            if (s < ReferenceMaster.instance.gridSystem.gridCollumns)
            {
                validCollumns.Add(s);
                //Debug.Log("Val " + s);
            }
            s = currentCollumn - i;
            if (s >= 0)
            {
                validCollumns.Add(s);
                //Debug.Log("Val " + s);
            }
        }
        
        for (int i = 0; i < validRows.Count ; i++)
        {
            for (int j = minCollumn; j <= maxCollumn; j++)
            {
                int x = (validRows[ i ]* ReferenceMaster.instance.gridSystem.gridCollumns) + j ;
                GridAttributes grid = ReferenceMaster.instance.gridSystem.gridsPresent[x].GetComponent<GridAttributes>();
                if (grid.TargettedOrNot() || grid.OccupiedOrNot() || grid.BlockedOrNot())
                {

                }
                else
                {
                    validGrids.Add(grid);
                    if (cube)
                    {
                        Instantiate(cube, grid.transform.position, grid.transform.rotation);
                    }
                }

            }
        }
        for (int i = 0; i < validCollumns.Count; i++)
        {
            for (int j = minRow; j <= maxRow; j++)
            {
                int x = (j * ReferenceMaster.instance.gridSystem.gridCollumns) + validCollumns[i];
                GridAttributes grid = ReferenceMaster.instance.gridSystem.gridsPresent[x].GetComponent<GridAttributes>();
                if(grid.TargettedOrNot() || grid.OccupiedOrNot() || grid.BlockedOrNot())
                {

                }
                else
                {
                    validGrids.Add(grid);
                    if (cube)
                    {
                        Instantiate(cube, grid.transform.position, grid.transform.rotation);
                    }
                }
                
                //Instantiate(cube, grid.transform.position, grid.transform.rotation);
            }
        }
        validGridsToMove = validGrids;
        result = GetOneEmptyFromValidGrid( validGrids);
        //Debug.Log("RaycastIndex "+ result.gridIndex);
        //if(validGrids.Count >0)
        //{
        //    int x = UnityEngine.Random.Range(0, validGrids.Count);
        //    result = validGrids[x];
        //}
    
        return result;
    }
    GridAttributes GetOneEmptyFromValidGrid(List<GridAttributes> validGrids)
    {
        GridAttributes result = null;
        if (validGrids.Count > 0)
        {
            int x = UnityEngine.Random.Range(0, validGrids.Count);
            result = validGrids[x];
            if (result.TargettedOrNot())
            {
                Debug.Log("Slapped Hard ");
            }
            if (result.OccupiedOrNot() )
            {
                Debug.Log("Slapped ");
                result = null;
                Attempts += 1;
                result = GetOneEmptyFromValidGrid(validGrids);
            }
        }
        return result;
    }

    public void PauseMoving()
    {
        if (!destined && walk)
        {
            paused = true;
        }
    }
    public void ResumeMoving()
    {
        if (!destined)
        {
            paused = false;
        }
    }

}
