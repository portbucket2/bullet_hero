﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameStatesControl : MonoBehaviour
{
    [SerializeField]
    GameStates gameState;
    public Action ActionMenuIdle    ;
    public Action ActionGameStarted ;
    public Action ActionGamePaused  ;
    public Action ActionLevelStarted;
    public Action ActionLevelEnded  ;
    public Action ActionGameOver    ;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void MenuIdleFun() { }
    public void GameStartedFun() { }
    public void GamePausedFun() { }
    public void LevelStartedFun() { }
    public void LevelEndedun() { }
    public void GameOverFun() { }
    



    public void GameStateChangeTo(GameStates gameStatee)
    {
        gameState = gameStatee;
        switch (gameState)
        {
            case GameStates.MenuIdle:
                {
                    ActionMenuIdle    ?.Invoke();
                    MenuIdleFun();
                    break;
                }
            case GameStates.GameStarted :
                {
                    ActionGameStarted?.Invoke();
                    GameStartedFun();
                    break;
                }
            case GameStates.GamePaused  :
                {
                    ActionGamePaused?.Invoke();
                    GamePausedFun();
                    break;
                }
            case GameStates.LevelStarted:
                {
                    ActionLevelStarted?.Invoke();
                    LevelStartedFun();
                    break;
                }
            case GameStates.LevelEnded:
                {
                    ActionLevelEnded?.Invoke();
                    LevelEndedun();
                    break;
                }
            case GameStates.GameOver    :
                {
                    ActionGameOver?.Invoke();
                    GameOverFun();
                    break;
                }

        }
    }
}
