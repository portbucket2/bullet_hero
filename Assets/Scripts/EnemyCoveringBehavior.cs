﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCoveringBehavior : MonoBehaviour
{
    public EnemyPathFinding enemyPathFinding;
    public EnemyShooting enemyShooting;
    public float hideDuration;
    public float shootDuration;
    public int cycleIndexx;
    public int maxCyclee;



    void Start()
    {
       
        enemyPathFinding = GetComponent<EnemyPathFinding>();
        enemyShooting = GetComponent<EnemyShooting>();


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            EnemyCovereingFuntion();
        }
    }

    public void EnemyCovereingFuntion()
    {
        EnemyTypeBasedOnCovering enemyTypee = enemyPathFinding.enemyType;
        
       //switch (enemyTypee)
       //{
       //    case EnemyType.moving:
       //        {
       //            
       //            break;
       //        }
       //    case EnemyType.covering:
       //        {
       //            
       //            break;
       //        }
       //
       //}
       if(enemyTypee == EnemyTypeBasedOnCovering.covering)
        {
            ShootFromCoverAtInterVal();
        }
    }
    public void ShootFromCoverAtInterVal()
    {
        StartCoroutine(CoveredFireCoroutine(hideDuration, shootDuration,0, maxCyclee));
    }
    public void CoverDownFunction()
    {
        enemyShooting.StopFire();
        enemyPathFinding.enemyAnimatorController.CoverEnemyAnim();
        //Debug.Log("CoverDown");
    }
    public void UnCoverUpFunction()
    {
        enemyShooting.StartFire();
        //Debug.Log("UnCoverDown");
        enemyPathFinding.enemyAnimatorController.UnCoverEnemyAnim();
    }
    public bool LeaveTheCoverFunction()
    {
        //StopAllCoroutines();
        //enemyPathFinding.enemyAnimatorController.CoverEnemyAnim();
        bool res = CoverChangeToNext();
        if (res)
        {
            enemyPathFinding.enemyAnimatorController.RunEnemyAnim();
        }
        

        return res;


    }
    public IEnumerator CoveredFireCoroutine(float hideDuration, float shootDuration, int cycleIndex  , int maxCycle )
    {
        yield return new WaitForSeconds(0f);
        if (cycleIndex == maxCycle)
        {
            //LeaveTheCoverFunction();
            if (!LeaveTheCoverFunction())
            {
                StartCoroutine(CoveredFireCoroutine(hideDuration, shootDuration, 0, maxCycle));
            }
        }
        else
        {
            CoverDownFunction();
            yield return new WaitForSeconds(hideDuration);
            UnCoverUpFunction();
            yield return new WaitForSeconds(shootDuration);
            CoverDownFunction();
            cycleIndex += 1;
            StartCoroutine(CoveredFireCoroutine(hideDuration,shootDuration, cycleIndex, maxCycle));
        }
        
        
    }
    public bool CoverChangeToNext()
    {
        return enemyPathFinding.CoveringEnemyNextCoverObsFind();
    }
}
