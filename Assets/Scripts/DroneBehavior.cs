﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneBehavior : MonoBehaviour
{
    public GameObject droneSensor;
    public GameObject droneSensorHolder;
    EnemyPathFinding enemyPathFinding;
    EnemyShooting enemyShooting;
    public GameObject lastHitObj;
    public GameObject hitObj;
    public float sensorMaxRange;
    public float sensorMinRange;
    public float randomRotY;
    public float walkSpeed;
    public bool destined;
    public GridAttributes target;
    public GridAttributes previousTarget;
    public GridAttributes targetGrid;
    public float shootDuration;
    public bool randomSearchDone;
    public int searchAttempts;
    public GameObject pointer;
    public int rightOrLeft;
    //public float targetBlockDistanceInUnits;
    // Start is called before the first frame update
    void Start()
    {
        enemyPathFinding = GetComponent<EnemyPathFinding>();
        enemyShooting = GetComponent<EnemyShooting>();


        if (enemyPathFinding.drone)
        {
            droneSensor.gameObject.SetActive(true);
            droneSensorHolder.gameObject.SetActive(true);
            enemyPathFinding.initialAction += DestinationSearchTasks;
        }
        else
        {
            droneSensor.gameObject.SetActive(false);
            droneSensorHolder.gameObject.SetActive(false);
        }

        //DestinationSearchTasks();
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime * 20 * walkSpeed * 0.02f);
            if (transform.position == target.transform.position)
            {

                DestinedTasks();
                previousTarget = target;
                target = null;
                destined = true;
                
                StartCoroutine(DroneDestinationReachCoroutine());
            }
        }
    }
    public void SensorHolderRandomRotate()
    {
        randomRotY = Random.Range(-180, 180);
        droneSensorHolder.transform.localRotation = Quaternion.Euler(new Vector3(0, randomRotY, 0));
    }
    public void SensorHolderRotateBy(float f)
    {
        randomRotY += f;
        droneSensorHolder.transform.localRotation = Quaternion.Euler(new Vector3(0, randomRotY, 0));
    }
    public float GetSensorNearerPointBy(float f)
    {
        droneSensor.transform.localPosition -= new Vector3(0, 0, f);
        //targetBlockDistanceInUnits = humveeSensor.transform.localPosition.z;
        //Debug.Log("SES dis = " + targetBlockDistanceInUnits);
        return droneSensor.transform.localPosition.z;
    }
    public void SensorResetDistance()
    {
        droneSensor.transform.localPosition = new Vector3(0, 0, sensorMaxRange);
    }
    public void SearchForTarget()
    {
        searchAttempts += 1;
        if (searchAttempts > 24)
        {
            return;
        }
        if (!randomSearchDone)
        {
            //SensorResetDistance();
            SensorHolderRandomRotate();
            //RaycastForBlock();
            

            randomSearchDone = true;
        }
        else
        {
            //SensorResetDistance();
            if (rightOrLeft == 1)
            {
                SensorHolderRotateBy(15);
            }
            else
            {
                SensorHolderRotateBy(-15);
            }
        
            //RaycastForBlock();
        }
        GridAttributes targetGridd = null;
        while (droneSensor.transform.localPosition.z > sensorMinRange && targetGridd == null)
        {
            targetGridd = ReferenceMaster.instance.gridSystem.GetMyGridBelow(droneSensor.transform.position);
            if (targetGridd == null)
            {
                GetSensorNearerPointBy(1);
                SearchForTarget();
            }
            else
            {
                if (targetGridd.OccupiedInAirOrNot() )
                {
                    //Debug.Log("AirOccupied");
                    targetGridd = null;
                    GetSensorNearerPointBy(1);
                    SearchForTarget();
                }
                else if (targetGridd.TargettedInAirOrNot())
                {
                    //Debug.Log("AirOccupied");
                    targetGridd = null;
                    GetSensorNearerPointBy(1);
                    SearchForTarget();
                }
                else
                {
                    //Debug.Log(" NOt AirOccupied");
                    target = targetGridd;
                }
                    //if (targetGrid.OccupiedInAirOrNot())
                    //{
                    //    targetGridd = null;
                    //    GetSensorNearerPointBy(1);
                    //    SearchForTarget();
                    //}
                    //else
                    //{
                    //    target = targetGridd;
                    //}

                    //target = targetGridd;
            }
        }
        if (target == null)
        {
            SensorResetDistance();
            SearchForTarget();
        }
        else
        {
            if (previousTarget != null)
            {
                previousTarget.GridMakeAirEmpty();
                previousTarget.GridAirTargetRemove();
            }
        }
    }

    public void DestinationSearchTasks()
    {
        destined = false;
        rightOrLeft = Random.Range(0, 2);
        searchAttempts = 0;
        randomSearchDone = false;
        SensorResetDistance();
        SearchForTarget();
    }
    public void DestinedTasks()
    {
        target.GridOccupyAir();
        target.GridAirTarget();
        //HumveeShootingStart();
    }
    public IEnumerator DroneDestinationReachCoroutine()
    {

        enemyShooting.StartFire();
        yield return new WaitForSeconds(shootDuration);
        enemyShooting.StopFire();
        DestinationSearchTasks();


    }
}
