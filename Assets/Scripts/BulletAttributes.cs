﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletAttributes : MonoBehaviour
{
    public float velocity;
    public LayerMask reflectiveLayer;
    public int bulletDamage;
    public float bulletLifetime;
    // Start is called before the first frame update
    void Start()
    {

        bulletLifetime = ReferenceMaster.instance.gameValuesTwaekHelper.playerValues.bulletLifetime;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0,0, velocity* Time.deltaTime*10 * 0.2f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision.gameObject.layer);
        if (collision.gameObject.layer != 8 && collision.gameObject.layer != 10 && collision.gameObject.layer != 11)
        {
            
            return;
        }
        if ( collision.gameObject.layer == 10 )
        {
            Debug.Log(collision.gameObject.name);
            if (collision.gameObject.GetComponent<EnemyHealth>())
            {
                collision.gameObject.GetComponent<EnemyHealth>().DamageEnemy(bulletDamage);
            }
            
            
            //Destroy(this.gameObject);
            DestroyBullet();
            return;
        }
        //Debug.Log("COLLIDEDD");
        foreach (ContactPoint contact in collision.contacts)
        {
            //Debug.DrawRay(contact.point, contact.normal, Color.white);
            float angle = Vector3.SignedAngle(-transform.forward, contact.normal, Vector3.up);
            //Debug.Log(angle);
            Vector3 rot = Quaternion.ToEulerAngles(transform.rotation) * Mathf.Rad2Deg;
            transform.position = contact.point;
            transform.rotation = Quaternion.Euler(new Vector3(0, rot.y + (180 +(2*angle)) , 0));
        }
    }

    public void DestroyBullet(float t = 0)
    {
        StopAllCoroutines();
        StartCoroutine(BulleyDestroyCoroutine(t));
        

    }
    IEnumerator BulleyDestroyCoroutine(float t)
    {
        yield return new WaitForSeconds(t);
        PoolingSystemShawon.instance.bulletPoolHero.DestroyBackToPool(this.gameObject);
    }
    void OnEnable()
    {
        InitialOperation();
        DestroyBullet(bulletLifetime);
    }

    void InitialOperation()
    {
        PlayerWeaponControl playerWeaponControl = ReferenceMaster.instance.playerWeaponControl;
        velocity = playerWeaponControl.bulletSpeed;
        bulletDamage = playerWeaponControl.bulletDamage;
    }

    
}
