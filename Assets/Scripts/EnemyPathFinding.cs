﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyPathFinding : MonoBehaviour
{
    public EnemyTypeBasedOnCovering enemyType;
    public bool rpg;
    public bool lmg;
    public bool humvee;
    public bool drone;
    public bool tripod;
    public bool bossSwat;
    public bool bossTank;
    public GridAttributes target;
    public GameObject raycastTarget;
    //public GridAttributes previousTarget;
    public float walkSpeed;
    public bool initialized;
    public bool walk;
    [SerializeField]
    bool paused;
    public bool destined;
    public LayerMask rayCastLayer;
    //public LayerMask enemyLayer;
    public GameObject hitObj;
    public GameObject lastHitObj;
    public GridAttributes currentGridBelow;
    public ObstacleWall wallToCover;
    public CoverPointAttributes coverPointCurrent;
    public Action initialAction;
    public Action lmgDestinedAction;
    public Action movingEndedAction;
    public bool moveEnded;
    public bool doubleChecked;
    public bool coverReached;
    public GridAttributes gridDoubleCheck;
    bool lmgStandUpdated;
    public EnemyAnimatorController enemyAnimatorController;
    public int currentGridExitIndex;
    // Start is called before the first frame update
    void Start()
    {
        enemyAnimatorController.RunEnemyAnim();

    }

    // Update is called once per frame
    void Update()
    {
        if ((humvee|| drone || bossSwat || bossTank) && initialized)
        {
            return;
        }
        if (paused)
        {
            //return;
        }
        if (destined && !walk && !moveEnded )
        {
            moveEnded = true;
            movingEndedAction?.Invoke();
        }

        Walk();
        
        //RaycastToPlayer();
        //if (Input.GetKeyDown(KeyCode.C))
        //{
        //    if (enemyType == EnemyType.covering)
        //    {
        //        CoveringEnemyNextCoverObsFind();
        //    }
        //        
        //}
    }

    public void Walk()
    {
        if (walk && target != null )
        {
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime * 20 * walkSpeed * 0.02f);
            if(lmg && paused )
            {
                if (!lmgStandUpdated)
                {
                    
                    target = ReferenceMaster.instance.gridSystem.GetMyGridBelow(transform.position);
                    target = target.GetComponent<GridAttributes>().GridSerachEmptyOne(target);
                    currentGridBelow = target;
                    if (!currentGridBelow.OccupiedOrNot())
                    {
                        currentGridBelow.GridOccupy();
                    }
                    lmgStandUpdated = true;
                }
                
                //paused = false;
                return;
            }
            if (transform.position == target.transform.position)
            {
                if (!initialized)
                {
                    if (ReferenceMaster.instance.enemySpawner.CompareInitialGrid(target))
                    {
                        InitialTasks();
                        initialized = true;
                        return;
                    }
                    
                }
                CoverPointReachCheck();
                //if (destined)
                //{
                //    //Debug.Log("THisIS NO JOKE");
                //}
                walk = false;
                target = null;

                currentGridBelow = ReferenceMaster.instance.gridSystem.GetMyGridBelow (transform.position);
                if (currentGridBelow != null)
                {
                    if(currentGridBelow.gridRowIndex == 0)
                    {
                        //DestinationGrab();
                    }

                    if (currentGridBelow.OccupiedOrNot() && enemyType != EnemyTypeBasedOnCovering.covering)
                    {
                        //Debug.Log("I GOT OCCUPIED " + this.gameObject.name);
                        walk = true;
                        
                        target = ReferenceMaster.instance.gridSystem.GetMyGrid(transform.position);
                    }
                    else
                    {
                        currentGridBelow.GridOccupy();
                        //Debug.Log("I OCCUPIED " + this.gameObject.name +" "+ currentGridBelow.gridIndex);
                    }
                    
                }

                
            }
        }
        else
        {
            if (!destined)
            {
                RaycastToRaycastTarget(raycastTarget);
            }
            
        }
        //else if(!destined)
        //{
        //    if(RaycastToPlayer()!= null)
        //    {
        //        
        //        
        //        if (currentGridBelow != null)
        //        {
        //            currentGridBelow.GridMakeEmpty();
        //        }
        //        currentGridBelow = RaycastToPlayer();
        //        if(target != null && target.gridRowIndex >= currentGridBelow.gridRowIndex)
        //        {
        //            destined = true;
        //            //target = currentGridBelow;
        //        }
        //        else
        //        {
        //            target = currentGridBelow;
        //            walk = true;
        //        }
        //        
        //        
        //    }
        //}

        
    }
    public bool PausedOrNot()
    {
        return paused;
    }
    public void PauseMoving()
    {
        paused = true;
    }
    public void ResumeMoving()
    {
        paused = false ;
        lmgStandUpdated = false;
        if(currentGridBelow != null && !moveEnded)
        {
            currentGridBelow.GridMakeEmpty();
        }
        
    }
    public bool DestinedOrNotCompare()
    {
        //bool destined = false;
        return destined;
    }
    public void DestinationGrab()
    {
        if (currentGridBelow != null)
        {
            currentGridBelow.GridOccupy();
        }
        destined = true;
        movingEndedAction?.Invoke();
        //Debug.Log("I Destined " + this.gameObject.name);
    }
    public void DestinationLeave()
    {
        destined = false;
        walk = true;
    }
    public GridAttributes RaycastToRaycastTarget(GameObject raycastTarget)
    {
        RaycastHit hit;
        if (hitObj)
        {
            lastHitObj = hitObj;
        }
        
        hitObj = null ;
        float hitDistance = 0;
        bool hitted = false;
        Vector3 dir = raycastTarget.gameObject.transform.position - transform.position;
        float rayLenth = Vector3.Distance(transform.position, raycastTarget.gameObject.transform.position);
        GridAttributes gridAttrReturn = null;
        //Debug.Log("rayTarget " + raycastTarget.gameObject.name);

        if ((Physics.Raycast(transform.position, dir, out hit, rayLenth, rayCastLayer)))
        {
            
            hitDistance = hit.distance;
            hitObj = hit.transform.gameObject;
            
            hitted = true;
            //Debug.DrawRay(transform.position, dir, Color.yellow);
            Debug.Log("Did Hit " + hit.transform.gameObject.name);
        }
        if (hitted)
        {
            if( hitObj.layer == 10)
            {
                Debug.Log("Did Hit Enemy" + hit.transform.gameObject.name);
                GridAttributes targetGrid = ReferenceMaster.instance.gridSystem.GetMyGrid(hitObj.transform.position);
                gridAttrReturn = targetGrid.GridSerachEmptyOne(targetGrid);
            }
            else if (hitObj.layer == 11)
            {
                if (enemyType == EnemyTypeBasedOnCovering.covering)
                {
                    if(hit.transform.gameObject.GetComponent<ObstacleWall>() == wallToCover)
                    {
                        Debug.Log("Did Hit Cover" + hit.transform.gameObject.name);
                        coverPointCurrent = wallToCover.GetAnEmptyCoverPoint();
                        if(coverPointCurrent != null)
                        {
                            //coverPointCurrent.OccupyCover();
                            //coverPointCurrent.OccupiedBy = this.gameObject;
                            GridAttributes targetGrid = ReferenceMaster.instance.gridSystem.GetMyGridBelow(coverPointCurrent.transform.position);
                            //gridAttrReturn = targetGrid.GridSerachEmptyOne(targetGrid);
                            gridAttrReturn = targetGrid;

                            if (!doubleChecked)
                            {
                                gridDoubleCheck = RayCastDoubleCheck(gridAttrReturn.gameObject);
                                if (gridDoubleCheck != null && gridDoubleCheck != gridAttrReturn)
                                {
                                    gridAttrReturn = gridDoubleCheck;
                                    Debug.Log("Second Tar " + gridAttrReturn.gridIndex);
                                }
                                else
                                {
                                    coverPointCurrent.OccupyCover();
                                    coverPointCurrent.OccupiedBy = this.gameObject;
                                }

                                //doubleChecked = true;
                            }
                            
                            //DestinationGrab();
                        }
                        
                    }
                    else
                    {
                        Debug.Log("Did Hit Obstacle" + hit.transform.gameObject.name);
                        if (hitObj == lastHitObj && currentGridExitIndex >= 0)
                        {
                            
                            gridAttrReturn = hit.transform.gameObject.GetComponent<ObstacleWall>().GeNexttGridExit(currentGridExitIndex);
                        }
                        else
                        {
                            GridAttributes gridExitt = hit.transform.gameObject.GetComponent<ObstacleWall>().GetGridNearestExit(transform.position);
                            currentGridExitIndex = hit.transform.gameObject.GetComponent<ObstacleWall>().GetExitPointIndex(gridExitt);
                            //gridAttrReturn = gridExitt.GridSerachEmptyOne(gridExitt);
                            gridAttrReturn = gridExitt;
                        }
                        




                    }
                }
                else
                {
                    Debug.Log("Did Hit Obstacle" + hit.transform.gameObject.name);
                    GridAttributes gridExitt = null;
                    if (hitObj == lastHitObj && currentGridExitIndex >= 0)
                    {
                        if (currentGridBelow == hit.transform.gameObject.GetComponent<ObstacleWall>().gridExits[currentGridExitIndex])
                        {
                            gridExitt = hit.transform.gameObject.GetComponent<ObstacleWall>().GeNexttGridExit(currentGridExitIndex);
                        }
                        else
                        {
                            gridExitt = hit.transform.gameObject.GetComponent<ObstacleWall>().gridExits[currentGridExitIndex];
                        }
                        //gridExitt = hit.transform.gameObject.GetComponent<ObstacleWall>().GeNexttGridExit(currentGridExitIndex);
                    }
                    else
                    {
                        gridExitt = hit.transform.gameObject.GetComponent<ObstacleWall>().GetGridNearestExit(transform.position);
                    }
                    
                    currentGridExitIndex = hit.transform.gameObject.GetComponent<ObstacleWall>().GetExitPointIndex(gridExitt);
                    //gridAttrReturn = gridExitt.GridSerachEmptyOne(gridExitt);
                    gridAttrReturn = gridExitt;
                    Debug.Log("First Tar " + gridAttrReturn.gridIndex);
                    if (!doubleChecked)
                    {
                        gridDoubleCheck = RayCastDoubleCheck(gridAttrReturn.gameObject);
                        if(gridDoubleCheck != null && gridDoubleCheck != gridAttrReturn)
                        {
                            gridAttrReturn = gridDoubleCheck;
                            Debug.Log("Second Tar " + gridAttrReturn.gridIndex);
                        }
                        
                        //doubleChecked = true;
                    }
                    
                    //if (gridDoubleCheck != null && gridDoubleCheck != gridAttrReturn)
                    //{
                        //gridAttrReturn = gridDoubleCheck;
                    //}
                    //gridAttrReturn = hit.transform.gameObject.GetComponent<ObstacleWall>().gridExit.GridSerachEmptyOne(hit.transform.gameObject.GetComponent<ObstacleWall>().gridExit);
                }
                
            }
            else if (hitObj.layer == 12)
            {
                if (rpg)
                {
                    DestinationGrab();
                    LMGReachedDestinamtionCheck();
                }
                else
                {
                    Debug.Log("Did Hit Player" + hit.transform.gameObject.name);
                    GridAttributes targetGrid = ReferenceMaster.instance.gridSystem.GetMyGrid(hitObj.transform.position, true);
                    gridAttrReturn = targetGrid.GridSerachEmptyOne(targetGrid);
                    if (currentGridBelow.gridRowIndex == 0)
                    {
                        DestinationGrab();
                        gridAttrReturn = null;
                    }

                    if (!doubleChecked && gridAttrReturn != null)
                    {
                        gridDoubleCheck = RayCastDoubleCheck(gridAttrReturn.gameObject);
                        if (gridDoubleCheck != null && gridDoubleCheck != gridAttrReturn)
                        {
                            gridAttrReturn = gridDoubleCheck;
                            Debug.Log("Second Tar " + gridAttrReturn.gridIndex);
                        }

                        //doubleChecked = true;
                    }
                }
                
            }
            //else if (hitObj.layer == 13)
            //{
            //    if(enemyType == EnemyType.covering )
            //    {
            //        if ( raycastTarget.transform.position == hitObj.transform.position)
            //        {
            //            Debug.Log("Did Hit Cover" + hit.transform.gameObject.name);
            //            GridAttributes targetGrid = ReferenceMaster.instance.gridSystem.GetMyGrid(hitObj.transform.position);
            //            gridAttrReturn = targetGrid.GridSerachEmptyOne(targetGrid);
            //            DestinationGrab();
            //        }
            //        else
            //        {
            //            Debug.Log("Did Hit Cover" + hit.transform.gameObject.name);
            //            GridAttributes targetGrid = ReferenceMaster.instance.gridSystem.GetMyGrid(raycastTarget.transform.position);
            //            gridAttrReturn = targetGrid.GridSerachEmptyOne(targetGrid);
            //            DestinationGrab();
            //        }
            //    }
            //    
            //}
        }
        else
        {
            Debug.Log("Did Not Hit ");
        }
        

        if(gridAttrReturn != null)
        {
            if(currentGridBelow != null && (gridAttrReturn.gridRowIndex == currentGridBelow.gridRowIndex )&& (gridAttrReturn.gridRowIndex== ReferenceMaster.instance.gridSystem.FrontlineDetermine()))
            {
                DestinationGrab();
            }
            else
            {
                
                target = gridAttrReturn;
                
                walk = true;
                if (currentGridBelow != null)
                {
                    currentGridBelow.GridMakeEmpty();
                }
            }
            
             
        }
        return gridAttrReturn;
    }
    public GridAttributes RayCastDoubleCheck(GameObject raycastTarget)
    {
        RaycastHit hit;
        if (hitObj)
        {
            lastHitObj = hitObj;
        }
        hitObj = null;
        float hitDistance = 0;
        bool hitted = false;
        Vector3 dir = raycastTarget.gameObject.transform.position - transform.position;
        float rayLenth = Vector3.Distance(transform.position, raycastTarget.gameObject.transform.position);
        GridAttributes gridAttrReturn = null;
        Debug.Log("rayTarget D " + raycastTarget.gameObject.name);

        if ((Physics.Raycast(transform.position, dir, out hit, rayLenth, rayCastLayer)))
        {

            hitDistance = hit.distance;
            hitObj = hit.transform.gameObject;
            hitted = true;
            Debug.DrawRay(transform.position, dir, Color.yellow);
            Debug.Log("Did Hit D " + hit.transform.gameObject.name);
        }
        if (hitted)
        {
            if(hitObj.layer == 11)
            {
                GridAttributes gridExitt = null;
                if (hitObj == lastHitObj && currentGridExitIndex >= 0)
                {
                    //if(currentGridBelow == hit.transform.gameObject.GetComponent<ObstacleWall>().gridExits[currentGridExitIndex])
                    //{
                    //    gridExitt = hit.transform.gameObject.GetComponent<ObstacleWall>().GeNexttGridExit(currentGridExitIndex);
                    //}
                    //else
                    //{
                    //    gridExitt = hit.transform.gameObject.GetComponent<ObstacleWall>().gridExits[currentGridExitIndex];
                    //}
                    gridExitt = hit.transform.gameObject.GetComponent<ObstacleWall>().GeNexttGridExit(currentGridExitIndex);
                }
                else
                {
                    gridExitt = hit.transform.gameObject.GetComponent<ObstacleWall>().GetGridNearestExit(transform.position);
                }
                currentGridExitIndex = hit.transform.gameObject.GetComponent<ObstacleWall>().GetExitPointIndex(gridExitt);
                //gridAttrReturn = gridExitt.GridSerachEmptyOne(gridExitt);
                gridAttrReturn = gridExitt;
            }
        }
        return gridAttrReturn;
    }
    public bool CoveringEnemyNextCoverObsFind()
    {
        bool res = false;
        moveEnded = false;
        if (ReferenceMaster.instance.obstacleManager.NextCoverObsFind(wallToCover) != null)
        {
            if (wallToCover != null && coverPointCurrent != null)
            {
                coverPointCurrent.EmptyCover();
                coverPointCurrent = null;
            }
            wallToCover = ReferenceMaster.instance.obstacleManager.NextCoverObsFind(wallToCover);
            raycastTarget =  wallToCover.gameObject;
            //
            //raycastTarget = wallToCover.GetAnEmptyCoverPoint().gameObject;
            //
            if (wallToCover.GetAnEmptyCoverPoint() != null)
            {
                destined = false;
                walk = true;
                res = true;
                //wallToCover.GetAnEmptyCoverPoint().OccupyCover();
            }
        }
        return res;
    }
    public void CoveringEnemyInitialCoverObsFind()
    {
        wallToCover = ReferenceMaster.instance.obstacleManager.InitialCoverObsFind(transform.position);
        //coverPointCurrent = wallToCover.GetAnEmptyCoverPoint();
        //coverPointCurrent.OccupyCover();
        //GridAttributes targetGrid = ReferenceMaster.instance.gridSystem.GetMyGrid(coverPointCurrent.transform.position);
        //target = targetGrid.GridSerachEmptyOne(targetGrid);
        raycastTarget = wallToCover.gameObject;
        //
        //raycastTarget = wallToCover.GetAnEmptyCoverPoint().gameObject;
        //
        //walk = true;
        //DestinationGrab();
    }

    public void InitialTasks()
    {
        raycastTarget = ReferenceMaster.instance.playerWeaponControl.gameObject;
        enemyAnimatorController.RunEnemyAnim();
        if (enemyType == EnemyTypeBasedOnCovering.covering)
        {
            CoveringEnemyInitialCoverObsFind();
        }
        initialAction?.Invoke();
    }
    public void SpawnTargetGridAssign(GridAttributes grid)
    {
        target = grid;
        walk = true;
    }
    public void EnemyDeathNote()
    {
        if(currentGridBelow != null)
        {
            currentGridBelow.GridMakeEmpty();
        }
        if(coverPointCurrent != null)
        {
            coverPointCurrent.EmptyCover();
            
        }
        ReferenceMaster.instance.enemySpawner.CountEnemiesRest(this.gameObject);
        ReferenceMaster.instance.gameManagementMain.ScoreUpdateBy(1);
        ReferenceMaster.instance.uiDummy.UIUpdate();

        
        GetComponent<EnemyFacingDirectionControl>(). EnemyDeathWaitTillAnimation(3);
        Destroy(this.gameObject);
    }
    public void CoverPointReachCheck()
    {
        coverReached = false;
        currentGridBelow = ReferenceMaster.instance.gridSystem.GetMyGridBelow(transform.position);
        if (enemyType == EnemyTypeBasedOnCovering.covering)
        {
            if (coverPointCurrent != null && Vector3.Distance( coverPointCurrent.transform.position , transform.position)< 0.1f)
            {
                coverReached = true;
                transform.position = coverPointCurrent.transform.position;
                GetComponent<EnemyCoveringBehavior>().EnemyCovereingFuntion();
                DestinationGrab();
            }
        }
    }
    public void LMGReachedDestinamtionCheck()
    {
        if (rpg && destined)
        {
            lmgDestinedAction?.Invoke();
            //Debug.Log("I AM LMG");
        }
        //Debug.Log("I AM LMG OUT");
    }
}
