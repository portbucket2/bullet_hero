﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchInputSystem : MonoBehaviour
{
    
    public event Action actionTouchedIn;
    public event Action actionTouchHolded;
    public event Action actionTouchedOut;
    public bool touched;
    public Vector2 previousMousePos;
    public Vector2 currentMousePos;
    public Vector2 progressionThisFrame;
    public float multiplier;
    public bool touchEnabled;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (touchEnabled)
        {
            if (Input.GetMouseButtonDown(0))
            {
                FuntionTouchedIn();
            }
            if (Input.GetMouseButton(0))
            {
                FuntionTouchHolded();
            }
            if (Input.GetMouseButtonUp(0))
            {
                FuntionTouchedOut();
            }
        }
        
        
    }

    void FuntionTouchedIn()
    {
        actionTouchedIn ?.Invoke();
        previousMousePos = Input.mousePosition;
        currentMousePos = Input.mousePosition;
        progressionThisFrame = Vector2.zero;
    }
    void FuntionTouchHolded()
    {
        actionTouchHolded?.Invoke();
        currentMousePos = Input.mousePosition;
        progressionThisFrame = ((currentMousePos - previousMousePos)*multiplier)/Screen.width;
        previousMousePos = currentMousePos;
    }
    void FuntionTouchedOut()
    {
        actionTouchedOut?.Invoke();
    }
    public void EnableTouch()
    {
        touchEnabled = true;
    }
    public void DisableTouch()
    {
        touchEnabled =  false;
        FuntionTouchedOut();
    }
}
