﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelProgressionEnemyInfo : MonoBehaviour
{
    //public List<GameObject> Level1Enemies;
    //public List<GameObject> Level2Enemies;
    //public List<GameObject> Level3Enemies;
    //public List<GameObject> Level4Enemies;
    public List<levelEnemies> levelEnemiesList;
    [System.Serializable]
    public struct levelEnemies
    {
        public string lvlInfo;
        public List<GameObject> enemies;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
}
