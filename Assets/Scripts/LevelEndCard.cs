﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LevelEndCard : MonoBehaviour
{
    public LevelEndCardTypes levelEndCardType;
    public Sprite[] cardSprites;
    //public string[] cardTitles;
    public Image[] cardImagesAjaira;
    public Image cardImageFinal;
    public Action actionCardClick;
    //public String[] cardTitles;
    public Text textCardTitle;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(ButtonFunction);
        GetComponent<Button>().onClick.AddListener(ReferenceMaster.instance.uiDummy.LevelEndCardClickUIFun);

        UpdateAjairaCardsImage();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CardTitleUpdate()
    {
        //for (int i = 0; i < cardTitles.Length; i++)
        //{
        //    cardTitles[i] = ((LevelEndCardTypes)i).ToString();
        //}
        textCardTitle.text = ""+ levelEndCardType.ToString();
    }
    public void CardImageUpdate(int finalIndex)
    {
        //for (int i = 0; i < cardTitles.Length; i++)
        //{
        //    cardTitles[i] = ((LevelEndCardTypes)i).ToString();
        //}
        cardImageFinal.sprite = cardSprites[finalIndex];
}
    public void ButtonFunction()
    {
        actionCardClick?.Invoke();
    }

    private void OnEnable()
    {
        GetComponent<Button>().enabled = false;
        Action act = null;
        act += EnableMeButton;
        GetComponentInChildren<UiTransitionCustom>().TransitionUpOrDownToCenter(-3000,0,1,act);
    }

    void UpdateAjairaCardsImage()
    {
        for (int i = 0; i < cardImagesAjaira.Length; i++)
        {
            int a = UnityEngine.Random.Range(0, cardSprites.Length);
            cardImagesAjaira[i].sprite = cardSprites[a];
        }
    }

    public void Test()
    {
        Debug.Log("ActionMovie");
    }
    public void EnableMeButton()
    {
        GetComponent<Button>().enabled = true;
    }
}
