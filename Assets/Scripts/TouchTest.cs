﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TouchTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ReferenceMaster.instance.touchInputSystem.actionTouchedIn += TouchInFunTest;
        ReferenceMaster.instance.touchInputSystem.actionTouchHolded += TouchHoldFunTest;
        ReferenceMaster.instance.touchInputSystem.actionTouchedOut += TouchOutFunTest;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void TouchInFunTest()
    {
        //Debug.Log("INNNNN");
    }
    public void TouchHoldFunTest()
    {
        //Debug.Log("HOLDDDDD");
    }
    public void TouchOutFunTest()
    {
        //Debug.Log("OUTTTTT");
    }
}
