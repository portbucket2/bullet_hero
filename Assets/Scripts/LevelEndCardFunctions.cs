﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LevelEndCardFunctions : MonoBehaviour
{

    public int cardTypesCount;
    public LevelEndCard[] levelEndCards;
    public List<int> cardIndexes;
    public List<int> selectedCardIncedexes;
    
    public InvalidCard[] invalidCards;
    public List<int> invalidCardIndexes;
    // Start is called before the first frame update
    [Serializable]
    public class InvalidCard
    {
        public LevelEndCardTypes cardType;
    }
    void Start()
    {
        for (int i = 0; i < invalidCards.Length; i++)
        {
            invalidCardIndexes.Add((int)invalidCards[i].cardType);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CardFunctionAssaultRifle      () { ReferenceMaster.instance.playerValuesAssignHelper.CardFunctionAssaultRifle(); }
    public void CardFunctionRPG               () {ReferenceMaster.instance.playerValuesAssignHelper.CardFunctionRPG(); }
    public void CardFunctionReloadtimeDecrease() {ReferenceMaster.instance.playerValuesAssignHelper.CardFunctionReloadTime(); }
    public void CardFunctionFireRateIncrease  () {ReferenceMaster.instance.playerValuesAssignHelper.CardFunctionFireRateIncrease25(); }
    public void CardFunctionShieldIncrease    () {ReferenceMaster.instance.playerValuesAssignHelper.CardFunctionShieldIncrease50(); }
    public void CardFunctionHealthIncrease    () {ReferenceMaster.instance.playerValuesAssignHelper.CardFunctionHelathIncrease50(); }
    public void CardFunctionHealFull  ()         {ReferenceMaster.instance.playerValuesAssignHelper.CardFunctionHealFull(); }
    public void CardFunctionHealHalf()           {ReferenceMaster.instance.playerValuesAssignHelper.CardFunctionHealHalf(); }
    public void CardFunctionDamageIncrease()     { ReferenceMaster.instance.playerValuesAssignHelper.CardFunctionDamageIncrease25(); }

    public void AssignRightFunctionToCard(LevelEndCard card)
    {
        card.actionCardClick = null;
        //Action act = card.actionCardClick;
        Action act = null;
        LevelEndCardTypes cardType = card.levelEndCardType;
        switch (cardType)
        {
            case LevelEndCardTypes. AssaultRifle       : {act+=CardFunctionAssaultRifle      ; break; }
            case LevelEndCardTypes. RPG                : {act+=CardFunctionRPG               ; break; }
            case LevelEndCardTypes. ReloadtimeDecrease : {act+=CardFunctionReloadtimeDecrease; break; }
            case LevelEndCardTypes. FireRateIncrease   : {act+=CardFunctionFireRateIncrease  ; break; }
            case LevelEndCardTypes. ShieldIncrease     : {act+=CardFunctionShieldIncrease    ; break; }
            case LevelEndCardTypes. HealthIncrease     : {act+=CardFunctionHealthIncrease    ; break; }
            case LevelEndCardTypes. HealFull           : {act+=CardFunctionHealFull          ; break; }
            case LevelEndCardTypes. HealHalf           : {act+=CardFunctionHealHalf          ; break; }
            case LevelEndCardTypes. DamageIncrease     : {act+= CardFunctionDamageIncrease   ;  break; }
            
        }
        card.actionCardClick = act;
        card.CardTitleUpdate();
        card.CardImageUpdate((int)cardType);
    }

    public void RandomThreeCardsAssign()
    {
        List<int> nums = new List<int>();
        List<int> numsSelected = new List<int>();
        for (int i = 0; i <cardTypesCount; i++)
        {
            nums.Add(i);
        }
        cardIndexes = nums;
        for (int i = 0; i < invalidCards.Length; i++)
        {
            nums.Remove(invalidCardIndexes[i]);
        }
        //cardIndexes = nums;
        for (int i = 0; i <3; i++)
        {
            int s = UnityEngine.Random.Range(0, nums.Count);
            
            numsSelected.Add(nums[s]);
           
            levelEndCards[i].levelEndCardType = (LevelEndCardTypes)numsSelected[i];
            AssignRightFunctionToCard(levelEndCards[i]);
            nums.RemoveAt(s);
        }
        selectedCardIncedexes = numsSelected;
        

    }
}
