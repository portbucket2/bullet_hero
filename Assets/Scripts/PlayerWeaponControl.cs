﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponControl : MonoBehaviour
{
    public PlayerWeaponType playerCurrentWeapon;
    public GameObject weaponObj;
    public GameObject bulletMuzzle;
    //public GameObject bulletObj;
    public float fireInterval;
    public LayerMask touchGroundLayer;
    public LayerMask laserHitLayer;
    public bool tapHitted;
    PlayerAnimatorController playerAnimatorController;
    public float bulletSpeed;
    public int bulletDamage;
    public LineRenderer lr;
    
    // Start is called before the first frame update
    void Start()
    {
        //ReferenceMaster.instance.touchInputSystem.actionTouchHolded += WeaponRoatate;
        ReferenceMaster.instance.touchInputSystem.actionTouchHolded += WeaponInitialRoatate;
        ReferenceMaster.instance.touchInputSystem.actionTouchedIn += WeaponInitialRoatate; 
        ReferenceMaster.instance.touchInputSystem.actionTouchedIn += StartFire;
        ReferenceMaster.instance.touchInputSystem.actionTouchedOut += StopFire;
        ReferenceMaster.instance.touchInputSystem.actionTouchedOut += LaserLineRendererDisable;

        playerAnimatorController = GetComponent<PlayerAnimatorController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void WeaponInitialRoatate()
    {
        //float movement = ReferenceMaster.instance.touchInputSystem.progressionThisFrame.x;
        //Vector3 rot = Quaternion.ToEulerAngles(weaponObj.transform.rotation) * Mathf.Rad2Deg;
        //
        //
        //rot.y += movement;
        //rot.y = Mathf.Clamp(rot.y, -60f, 60f);
        Vector3 hitPOs = RaycastAtClickedPos();
        if (tapHitted)
        {
            weaponObj.transform.rotation = Quaternion.LookRotation(hitPOs - transform.position, Vector3.up);
            Vector3 rot = Quaternion.ToEulerAngles(weaponObj.transform.rotation) * Mathf.Rad2Deg;
            rot.y = Mathf.Clamp(rot.y, -60f, 60f);
            weaponObj.transform.rotation = Quaternion.Euler(rot);
        }
        
    }
    public void WeaponRoatate()
    {
        float movement = ReferenceMaster.instance.touchInputSystem.progressionThisFrame.x;
        Vector3 rot = Quaternion.ToEulerAngles(weaponObj.transform.rotation) * Mathf.Rad2Deg;
        
        //Debug.Log(rot);
        rot.y += movement;
        rot.y = Mathf.Clamp(rot.y, -60f, 60f);
        weaponObj.transform.rotation = Quaternion.Euler(rot);
    }
    void Fire()
    {
        //Instantiate(bulletObj, bulletMuzzle.transform.position, bulletMuzzle.transform.rotation);
        PoolingSystemShawon.instance.bulletPoolHero.InstantiateFromPool(bulletMuzzle.transform.position, bulletMuzzle.transform.rotation);
    }
    public void StopFire()
    {
        StopAllCoroutines();

        playerAnimatorController.IdleCrouchPlayerAnim();
    }
    public void StartFire()
    {
        StartCoroutine(FireCoroutine(fireInterval));
        RaycastAtClickedPos();

        playerAnimatorController.AttackPlayerAnim();
    }
    public IEnumerator FireCoroutine(float interval)
    {
        Fire();
        yield return new WaitForSeconds(interval);
        StartCoroutine(FireCoroutine(interval));
    }
    public Vector3 RaycastAtClickedPos()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); // Construct a ray from the current mouse coordinates
        RaycastHit hit ;
        Vector3 hitPos = new Vector3();
        
        tapHitted = false;
        if (Physics.Raycast(ray, out hit, touchGroundLayer))
        {
            //Debug.DrawLine(hit.point, debugrayend, Color.red);
            //Debug.Log(hit.point +" "+ hit.transform.gameObject);
            hitPos = new Vector3(hit.point.x, 0, hit.point.z);
            //hitObj = hit.transform.gameObject;
            tapHitted = true;

            LaserLineRendererFunction();
        }

        return hitPos;
    }
    public void LaserLineRendererFunction()
    {
        if(lr.enabled == false)
        {
            lr.enabled = true;
        }
        RaycastHit hitt;
        Vector3 hitPoss = new Vector3();

        
        if (Physics.Raycast(bulletMuzzle.transform.position, bulletMuzzle.transform.forward, out hitt,200f, laserHitLayer))
        {
            //Debug.DrawLine(hit.point, debugrayend, Color.red);
            //Debug.Log(hit.point +" "+ hit.transform.gameObject);
            hitPoss = new Vector3(hitt.point.x, 0, hitt.point.z);
            //hitObj = hit.transform.gameObject;
            Debug.Log(hitPoss);

            lr.SetPosition(0, bulletMuzzle.transform.position);
            lr.SetPosition(1, hitt.point);

        }
    }
    public void LaserLineRendererDisable()
    {
        lr.enabled = false;
    }
    public void SetPlayerFireInterval(float firePerSec)
    {
        if(firePerSec <= 0)
        {
            return;

        }
        fireInterval = 1 / (float)firePerSec;
    }
    public void FireRateIncreaseBy(float percentage)
    {

        fireInterval = ((100 - percentage) / 100) * fireInterval;
    }
    public void SetBulletDamage(int dmg)
    {
        bulletDamage = dmg;
    }
    public void IncreaseBulletDamageByPercentage(float percentage)
    {
        bulletDamage = (int)((float)bulletDamage *(1 + ((float)percentage)/100));
        
    }
    public void SetBulletSpeed(float speed)
    {
        bulletSpeed = speed;
    }
}
