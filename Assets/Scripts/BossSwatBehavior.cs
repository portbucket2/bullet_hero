﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSwatBehavior : MonoBehaviour
{
    public MoverToAnyPoint          moverToAnyPoint            ;
    //public SearchingForAGridInRange searchingForAGridInRange  ;
    public EnemyPathFinding         enemyPathFinding              ;
    public EnemyShooting            enemyShooting                ;
    public float standingDuration;
    public float movingApproxDuration;

    EnemyAnimatorController enemyAnimatorController;

    public Transform swatUpperBody;
    public Vector3 swatUpperBodyTargetOffset;
    public GameObject shieldObj;
    
    // Start is called before the first frame update
    void Start()
    {
        moverToAnyPoint          = GetComponent<MoverToAnyPoint         >();
        //searchingForAGridInRange = GetComponent<SearchingForAGridInRange>();
        enemyPathFinding         = GetComponent<EnemyPathFinding        >();
        enemyShooting =            GetComponent<EnemyShooting>();
        enemyAnimatorController = GetComponent<EnemyAnimatorController>();
        enemyPathFinding.initialAction += InitializeBoss;
        moverToAnyPoint.ActionDestinationReach += BossDestinamtionReachFun;
        moverToAnyPoint.ActionDestinationLeave += BossDestinamtionLeaveFun;

        

        //moverToAnyPoint.CurrentGridBelowDetermineAndOccupy();
        //moverToAnyPoint.raycastTarget = searchingForAGridInRange.SearchForAValidRaycastGridTarget(moverToAnyPoint.currentGridBelow).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            BossPositionChange();
        }
    }
    void InitializeBoss()
    {
        Debug.Log("BOSSSS");

        BossPositionChange();
        if (enemyPathFinding.bossTank)
        {
            //BossShootingStart();
        }
        //moverToAnyPoint.raycastTarget = searchingForAGridInRange.SearchForAValidRaycastGridTarget(moverToAnyPoint.currentGridBelow).gameObject;
        //moverToAnyPoint.InitializeMoving(searchingForAGridInRange.SearchForAValidRaycastGridTarget(moverToAnyPoint.currentGridBelow).gameObject);
    }

    public void BossPositionChange()
    {
        moverToAnyPoint.CurrentGridBelowDetermineAndOccupy();
        GridAttributes gr = moverToAnyPoint.SearchForAValidRaycastGridTarget(moverToAnyPoint.currentGridBelow);
        if (gr != null)
        {
            moverToAnyPoint.raycastTarget = gr.gameObject;
            moverToAnyPoint.InitializeMoving(moverToAnyPoint.raycastTarget);
        }
    }

    public void BossDestinamtionReachFun()
    {
        if (enemyPathFinding.bossTank)
        {
            StartCoroutine(BossSwatShootingCoRoutine());
        }
        if (enemyPathFinding.bossSwat)
        {
            BossShootingStop();
            StartCoroutine(BossTankShootingCoRoutine());

            enemyAnimatorController.IdleEnemyAnim();
            ShieldUp();
        }


    }
    public void BossDestinamtionLeaveFun()
    {
        if (enemyPathFinding.bossTank)
        {
           
        }
        if (enemyPathFinding.bossSwat)
        {
            //BossShootingStop();
            BossShootingStart();

            enemyAnimatorController.RunEnemyAnim();
            ShieldDown();
        }


    }
    public IEnumerator BossSwatShootingCoRoutine()
    {
        enemyShooting.StartFire();
        yield return new WaitForSeconds(standingDuration);
        enemyShooting.StopFire();
        BossPositionChange();

    }
    public IEnumerator BossTankShootingCoRoutine()
    {
        
        yield return new WaitForSeconds(standingDuration);
        
        BossPositionChange();

    }
    public void BossShootingStart()
    {
        BossShootingStop();
        enemyShooting.StartFire();
        Debug.Log("BOSSSS SHOOT");
    }
    public void BossShootingStop()
    {
        enemyShooting.StopFire();
    }
    public void ShieldUp()
    {
        if (shieldObj)
        {
            shieldObj.SetActive(true);
        }
    }
    public void ShieldDown()
    {
        if (shieldObj)
        {
            shieldObj.SetActive(false);
        }
    }

}
