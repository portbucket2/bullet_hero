﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HumveeBehavior : MonoBehaviour
{
    public GameObject humveeSensor;
    public GameObject humveeSensorHolder;
    EnemyPathFinding enemyPathFinding;
    EnemyShooting enemyShooting;
    public GameObject lastHitObj;
    public GameObject hitObj;
    public float sensorMaxRange;
    public float sensorMinRange;
    public float randomRotY;
    public float walkSpeed;
    public bool destined;
    public GridAttributes target;
    public GridAttributes previousTarget;
    public GridAttributes targetGrid;
    //public GridAttributes secondGrid;
    public float shootDuration;
    public bool randomSearchDone;
    public int searchAttempts;
    public GameObject pointer;
    //public GameObject humVeeObj;
   // public GameObject secondGridObj;
    public int rightOrLeft;
    //public bool frontBackOrientation;
    //Quaternion humveeFinalRot;
    //Quaternion humveeTargetRot;
    //public EnemyOrientation humveeOri;
    //public bool backGear;
    //public int reverseFac;
    //public int backGearFac;
    //bool transitionIn;
    //public float frac;
    //Quaternion oldRot;
    // Start is called before the first frame update
    public GameObject enemyMesh;
    public Action ActionSteerStart;
    public Action ActionSteerEnd;
    public Action ActionMoveStart;
    public Action ActionMoveEnd;
    bool steering;
    bool steerDone;

    public GameObject[] steerings;
    public GameObject[] wheels;
    public float steerAngle;
    public float wheelRot;
    public float wheelRotSpeed;
    float steerSign;
    public GameObject HumveeTurret;

    Quaternion rotTarget;
    Quaternion turretRotTar;
    bool turretRot;
    void Start()
    {
        enemyPathFinding = GetComponent<EnemyPathFinding>();
        enemyShooting = GetComponent<EnemyShooting>();
        //oldRot = humveeTargetRot = humveeFinalRot= Quaternion.LookRotation(enemyPathFinding.target.transform.position - transform.position, Vector3.up);

        if (enemyPathFinding.humvee)
        {
            humveeSensor.gameObject.SetActive(true);
            humveeSensorHolder.gameObject.SetActive(true);
            enemyPathFinding.initialAction += SearchForTarget;
            
        }
        else
        {
            humveeSensor.gameObject.SetActive(false);
            humveeSensorHolder.gameObject.SetActive(false);
        }

        ActionSteerStart += SteerStart;
        ActionSteerEnd += SteerStop;
        //ActionMoveStart +=  HumveeTurretAllign;
        ActionMoveEnd += HumveeTurretAim;
        //SearchForTarget();
    }

    // Update is called once per frame
    void Update()
    {
        if (!enemyPathFinding.humvee)
        {
            return;
        }
        if (Input.GetKeyDown(KeyCode.H) && enemyPathFinding.humvee && !target)
        {
            SensorResetDistance();
            SensorHolderRandomRotate();
            RaycastForBlock();
        }
        if (Input.GetKeyDown(KeyCode.J) && enemyPathFinding.humvee && !target)
        {
            SensorResetDistance();
            if (rightOrLeft == 1)
            {
                SensorHolderRotateBy(15);
            }
            else
            {
                SensorHolderRotateBy(-15);
            }
            SensorHolderRotateBy(15);
            RaycastForBlock();
        }
        if (!enemyPathFinding.initialized)
        {
            RotateWheels();
        }
        //HumveeRotationControl();
        if (target != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime * 20 * walkSpeed * 0.02f);
            //if(!transitionIn && Vector3.Distance(transform.position, target.transform.position)< 1)
            //{
            //    transitionIn = true;
            //    frac = 0;
            //    oldRot = humVeeObj.transform.rotation;
            //}
            if(enemyMesh && !steerDone )
            {
                if (!steering)
                {
                    steering = true;
                    ActionSteerStart?.Invoke();
                }
                rotTarget = Quaternion.LookRotation(target.transform.position - enemyMesh.transform.position, enemyMesh.transform.up);
                enemyMesh.transform.rotation = Quaternion.RotateTowards(enemyMesh.transform.rotation, rotTarget, Time.deltaTime * 100f);

                

                if (enemyMesh.transform.rotation == rotTarget){
                    ActionSteerEnd?.Invoke();
                    steerDone = true;
                }
            }
            steerings[0].transform.localRotation = Quaternion.Lerp(steerings[0].transform.localRotation, Quaternion.Euler(0, steerAngle, 0), Time.deltaTime *10);
            steerings[1].transform.localRotation = Quaternion.Lerp(steerings[1].transform.localRotation, Quaternion.Euler(0, steerAngle, 0), Time.deltaTime * 10);



            RotateWheels();

            if (transform.position == target.transform.position)
            {
                ActionMoveEnd?.Invoke();
                DestinedTasks();
                previousTarget = target;
                target = null;
                destined = true;
                steering = false;
                steerDone = false;
                
            }
        }
        if (turretRot)
        {
            HumveeTurret.transform.rotation = Quaternion.RotateTowards(HumveeTurret.transform.rotation, turretRotTar, Time.deltaTime * 300);
            if(HumveeTurret.transform.rotation == turretRotTar)
            {
                turretRot = false;
            }
        }
        
    }

    public void RotateWheels()
    {
        wheelRot += Time.deltaTime * wheelRotSpeed;
        if (wheelRot > 360)
        {
            wheelRot -= 360;
        }
        Quaternion rot = Quaternion.Euler(wheelRot, 0, 0);
        for (int i = 0; i < wheels.Length; i++)
        {
            wheels[i].transform.localRotation = rot;
        }
    }
    //public void HumveeRotationControl()
    //{
    //    if(oldRot == null)
    //    {
    //        oldRot = humveeTargetRot  =Quaternion.identity;
    //    }
    //    if (!transitionIn )
    //    {
    //        if (target)
    //        {
    //            if (backGear)
    //            {
    //                humveeTargetRot = Quaternion.LookRotation(-target.transform.position + transform.position, Vector3.up);
    //            }
    //            else
    //            {
    //                humveeTargetRot = Quaternion.LookRotation(target.transform.position - transform.position, Vector3.up);
    //            }
    //        }
    //        
    //    }
    //    else
    //    {
    //        humveeTargetRot = humveeFinalRot;
    //        //Debug.Log(rot);
    //    }
    //    frac = Mathf.MoveTowards(frac, 1, Time.deltaTime * walkSpeed * 0.4f);
    //    humVeeObj.transform.rotation = Quaternion.Lerp(oldRot, humveeTargetRot , frac);
    //
    //}
    //public Quaternion HumveeGetFinalRot(EnemyOrientation ori)
    //{
    //    Quaternion rot =  Quaternion.identity;
    //    transitionIn = false;
    //    frac = 0;
    //    oldRot = humVeeObj.transform.rotation;
    //    switch (ori)
    //    {
    //        case EnemyOrientation.right:
    //            {
    //                rot = Quaternion.Euler(new Vector3(0, ((reverseFac - backGearFac) * -180)+ 90, 0));
    //                break;
    //
    //            }
    //        case EnemyOrientation.left:
    //            {
    //                rot = Quaternion.Euler(new Vector3(0, ((reverseFac - backGearFac) * -180) + -90, 0));
    //                break;
    //
    //            }
    //        case EnemyOrientation.front:
    //            {
    //                rot = Quaternion.Euler(new Vector3(0, ((reverseFac - backGearFac) * -180) + 0, 0));
    //                break;
    //
    //            }
    //        case EnemyOrientation.back:
    //            {
    //                rot = Quaternion.Euler(new Vector3(0, ((reverseFac - backGearFac) * -180) + 180, 0));
    //                break;
    //
    //            }
    //
    //    }
    //    
    //    return rot;
    //}
    public void SearchForTarget()
    {
        searchAttempts += 1;
        if(searchAttempts > 24)
        {
            HumveeShootingStart();
            return;
        }
        if (!randomSearchDone)
        {
            SensorResetDistance();
            SensorHolderRandomRotate();
            RaycastForBlock();
            randomSearchDone = true;
        }
        else
        {
            SensorResetDistance();
            if(rightOrLeft == 1)
            {
                SensorHolderRotateBy(15);
            }
            else
            {
                SensorHolderRotateBy(-15);
            }
            
            RaycastForBlock();
        }
        if(target == null)
        {
            SearchForTarget();
        }
        else
        {
            if(previousTarget != null)
            {
                previousTarget.GridMakeEmpty();
            }
        }
    }
    //public IEnumerator DestinationSearchCoroutine()
    //{
    //    yield return new WaitForSeconds(2);
    //    
    //    DestinationSearchTasks();
    //
    //
    //}
    public GridAttributes HumveeSearchDestination()
    {
        GridAttributes targetGrid = null;
        return targetGrid;
    }
    public void SensorHolderRandomRotate()
    {
        randomRotY = UnityEngine. Random.Range(-180, 180);
        humveeSensorHolder.transform.localRotation = Quaternion.Euler(new Vector3(0, randomRotY, 0));
    }
    public void SensorHolderRotateBy(float f)
    {
        randomRotY += f;
        humveeSensorHolder.transform.localRotation = Quaternion.Euler(new Vector3(0, randomRotY, 0));
    }
    public GameObject RaycastForBlock()
    {
        RaycastHit hit;
        hitObj = null;
        targetGrid = null;
        
        Vector3 dir = humveeSensor.gameObject.transform.position - transform.position;
        float rayLenth = Vector3.Distance(transform.position, humveeSensor.gameObject.transform.position);
        
        //Debug.Log("rayTarget " + raycastTarget.gameObject.name);

        if ((Physics.Raycast(transform.position, dir, out hit, rayLenth, enemyPathFinding. rayCastLayer)))
        {

            
            hitObj = hit.transform.gameObject;
            
            //Debug.DrawRay(transform.position, dir, Color.yellow);
            //Debug.Log("Did Hit " + hit.transform.gameObject.name);
        }
        //
        if (hitObj != null)
        {
            lastHitObj = hitObj;
            
            if ( GetSensorNearerPointBy(1) >= sensorMinRange)
            {
                RaycastForBlock();
            }
            
        }
        else
        {
            targetGrid = ReferenceMaster.instance.gridSystem.GetMyGridBelow(humveeSensor.transform.position);
            if (targetGrid != null )
            {

                if (targetGrid.OccupiedOrNot())
                {

                    targetGrid = null;
                    if (GetSensorNearerPointBy(1) >= sensorMinRange)
                    {
                        RaycastForBlock();
                    }

                }
                else
                {
                    target = targetGrid;

                    ActionMoveStart?.Invoke();
                    //secondGrid = SearchSecondGrid(targetGrid);
                    //if(secondGrid != null)
                    //{
                    //    target = targetGrid;
                    //    if(target != HumveeGetTargetFromTwoGridOccupy(secondGrid))
                    //    {
                    //        GridAttributes realSecond = target;
                    //        target = secondGrid;
                    //        secondGrid = realSecond;
                    //    }
                    //
                    //    humveeFinalRot = HumveeGetFinalRot(humveeOri);
                    //}
                    //else
                    //{
                    //    targetGrid = null;
                    //    if (GetSensorNearerPointBy(1) >= sensorMinRange)
                    //    {
                    //        RaycastForBlock();
                    //    }
                    //}


                }
            }
            //target = targetGrid;
            if (enemyPathFinding.humvee)
            {
                //pointer.transform.position = humveeSensor.transform.position;
            }
            
        }
        

        return hitObj;
    }
    public float GetSensorNearerPointBy(float f)
    {
        humveeSensor.transform.localPosition -= new Vector3(0, 0, f);
        return humveeSensor.transform.localPosition.z;
    }
    public void SensorResetDistance()
    {
        humveeSensor.transform.localPosition = new Vector3(0, 0, sensorMaxRange);
    }

    public void HumveeShootingStart()
    {
        StartCoroutine(HumveeShootStartCoroutine());
    }
    public void DestinationSearchTasks()
    {
        destined = false;
        rightOrLeft = UnityEngine.Random.Range(0, 2);
        searchAttempts = 0;
        randomSearchDone = false;
        SearchForTarget();
    }
    public IEnumerator HumveeShootStartCoroutine()
    {
        enemyShooting.StartFire();
        yield return new WaitForSeconds(shootDuration);
        enemyShooting.StopFire();
        DestinationSearchTasks();
    }
    public void DestinedTasks()
    {
        target.GridOccupy();
        HumveeShootingStart();
    }
    public void SteerStart()
    {
        steerAngle = 35;
        SteerDirDetermine();
    }
    public void SteerStop()
    {
        steerAngle = 0;
    }
    public void SteerDirDetermine()
    {
        Quaternion rotTargett = Quaternion.LookRotation(target.transform.position - enemyMesh.transform.position, enemyMesh.transform.up);
        float rotNed = (Quaternion.ToEulerAngles(rotTargett) - Quaternion.ToEulerAngles(enemyMesh.transform.rotation)).y * Mathf.Rad2Deg;
        if (rotNed > 180)
        {
            rotNed = -(360 - rotNed);
        }
        else if (rotNed < -180)
        {
            rotNed = (360 - Mathf.Abs(rotNed));
        }
        //float negFac = 1;
        //if (!left)
        //{
        //    negFac = -1;
        //}
        if (rotNed > 0)
        {
            steerSign = 1;
        }
        else if (rotNed < 0)
        {
            steerSign = -1;
            //steerAngle *= -1;
        }
        else
        {
            //speedVar = speed;
        }
        steerAngle *= steerSign;
    }
    public void HumveeTurretAim()
    {
        turretRotTar = Quaternion.LookRotation(ReferenceMaster.instance.playerWeaponControl.transform.position - HumveeTurret.transform.position, Vector3.up);
        turretRot = true;
    }
    public void HumveeTurretAllign()
    {
        turretRotTar = enemyMesh.transform.rotation;
        turretRot = true;
    }
    //public GridAttributes HumveeGetTargetFromTwoGridOccupy(GridAttributes secondGr)
    //{
    //    GridAttributes targett = null;
    //    Vector3 avg = Vector3.Lerp(target.transform.position, secondGr.transform.position, 0.5f);
    //    Vector3 vecFr = humVeeObj. transform.InverseTransformPoint(avg);
    //    if (vecFr.z < 0)
    //    {
    //        backGear = true;
    //        backGearFac = 1;
    //    }
    //    else
    //    {
    //        backGear = false;
    //        backGearFac = 0;
    //    }
    //    if(Vector3.Distance(transform.position , target.transform.position) > Vector3.Distance(transform.position, secondGrid.transform.position))
    //    {
    //        reverseFac = 1;
    //        targett = secondGr;
    //    }
    //    else
    //    {
    //        reverseFac = 0;
    //        targett = target;
    //    }
    //
    //    return targett;
    //}
    //public GridAttributes SearchSecondGrid(GridAttributes targetGr)
    //{
    //    GridAttributes secondGrid = null;
    //    if (!frontBackOrientation)
    //    {
    //        if(targetGr.rightGrid!= null && !targetGr.rightGrid.OccupiedOrNot() && secondGrid == null)
    //        {
    //            secondGrid = targetGr.rightGrid;
    //            humveeOri = EnemyOrientation.right;
    //        }
    //        if (targetGr.leftGrid != null && !targetGr.leftGrid.OccupiedOrNot() && secondGrid == null)
    //        {
    //            secondGrid = targetGr.leftGrid;
    //            humveeOri = EnemyOrientation.left;
    //        }
    //        if(secondGrid != null)
    //        {
    //            frontBackOrientation = true;
    //        }
    //    }
    //    else
    //    {
    //        if (targetGr.frontGrid != null && !targetGr.frontGrid.OccupiedOrNot() && secondGrid == null)
    //        {
    //            secondGrid = targetGr.frontGrid;
    //            humveeOri = EnemyOrientation.front;
    //        }
    //        if (targetGr.backGrid != null && !targetGr.backGrid.OccupiedOrNot() && secondGrid == null)
    //        {
    //            secondGrid = targetGr.backGrid;
    //            humveeOri = EnemyOrientation.back;
    //        }
    //        if (secondGrid != null)
    //        {
    //            frontBackOrientation = false;
    //        }
    //    }
    //    
    //
    //    return secondGrid;
    //}
}
