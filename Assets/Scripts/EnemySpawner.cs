﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject leftSpawnPoint;
    public GameObject rightSpawnPoint;
    public GridAttributes leftEntranceGrid;
    public GridAttributes rightEntranceGrid;
    public bool rightSpawn;
    public GameObject enemyMoving;
    public GameObject enemyCovering;
    public GameObject enemyLMG;
    public List<GameObject> enemySpawnList;
    public List<GameObject> enemiesInCurrentLevel;
    public float EnemySpawnInterval;
    int enemySpawnedCount;
    // Start is called before the first frame update
    void Start()
    {
        leftEntranceGrid = ReferenceMaster.instance.gridSystem.leftEntryGrid;
        rightEntranceGrid = ReferenceMaster.instance.gridSystem.rightEntryGrid;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            
        }
    }
    public void EnemySpawnFunction(int index)
    {
        if(index == 0)
        {
            rightSpawn = false;
        }
        enemySpawnList = ReferenceMaster.instance.LevelProgressionEnemyInfo.levelEnemiesList[index].enemies;
        //switch (index)
        //{
        //    case 0:
        //        {
        //            enemySpawnList = GetComponent<LevelProgressionEnemyInfo>().Level1Enemies;
        //            break;
        //        }
        //    case 1:
        //        {
        //            enemySpawnList = GetComponent<LevelProgressionEnemyInfo>().Level2Enemies;
        //            break;
        //        }
        //    case 2:
        //        {
        //            enemySpawnList = GetComponent<LevelProgressionEnemyInfo>().Level3Enemies;
        //            break;
        //        }
        //    case 3:
        //        {
        //            enemySpawnList = GetComponent<LevelProgressionEnemyInfo>().Level4Enemies;
        //            break;
        //        }
        //
        //}

        EnemySpawnStart();
    }
    public void SpawnEnemy(GameObject go)
    {
        GameObject enemy = null;
        if (rightSpawn)
        {
            rightSpawn = false;
        }
        else
        {
            rightSpawn = true;
        }

        if (rightSpawn)
        {
            enemy =  Instantiate(go, rightSpawnPoint.transform.position, rightSpawnPoint.transform.rotation);
            enemy.GetComponent<EnemyPathFinding>().SpawnTargetGridAssign(rightEntranceGrid) ;
        }
        else
        {
            enemy = Instantiate(go, leftSpawnPoint.transform.position, leftSpawnPoint.transform.rotation);
            enemy.GetComponent<EnemyPathFinding>().SpawnTargetGridAssign(leftEntranceGrid);
        }
        enemiesInCurrentLevel.Add(enemy);
        enemySpawnedCount += 1;
        enemy.GetComponent<EnemyHealth>().enemySpawnIndex = enemiesInCurrentLevel.Count - 1;
    }

    public void SpawnMoving()
    {
        SpawnEnemy(enemyMoving);
    }
    public void SpawnCovering()
    {
        SpawnEnemy(enemyCovering);
    }
    public void SpawnLMG()
    {
        SpawnEnemy(enemyLMG);
    }

    public bool CompareInitialGrid(GridAttributes grid)
    {
        bool result = false;
        if(grid.gridIndex == leftEntranceGrid.gridIndex || grid.gridIndex == rightEntranceGrid.gridIndex)
        {
            result = true;
        }
        return result;
    }
    public void EnemySpawnStart()
    {

        enemySpawnedCount = 0;
        StartCoroutine(EnmenySpawnCoroutine(0, EnemySpawnInterval));
        
    }
    public IEnumerator EnmenySpawnCoroutine(int enemyIndex,float interval)
    {
        //yield return new WaitForSeconds(0.5f);
        
        if (enemyIndex < enemySpawnList.Count)
        {
            
            SpawnEnemy(enemySpawnList[enemyIndex]);
            
            enemyIndex += 1;
            yield return new WaitForSeconds(interval);
            StartCoroutine(EnmenySpawnCoroutine( enemyIndex, interval));
        }
        

    }
    public int CountEnemiesRest(GameObject go)
    {
        enemiesInCurrentLevel.Remove(go);
        int count = enemiesInCurrentLevel.Count;
        //for (int i = 0; i < enemySpawnList.Count; i++)
        //{
        //    if(enemySpawnList[i]!= null)
        //    {
        //        count += 1;
        //    }
        //}
        //Debug.Log("REMOVEDD " + index);
        if(count == 0 && enemySpawnedCount == enemySpawnList.Count)
        {
            StartCoroutine(ReferenceMaster.instance.levelManager.LevelEndCoroutine(2));
            //ReferenceMaster.instance.levelManager.LevelEnd();

        }
        return count;
    }
}
