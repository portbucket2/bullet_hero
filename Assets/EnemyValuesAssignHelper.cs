﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyValuesAssignHelper : MonoBehaviour
{
    public EnemyType enemyType;
    public GameValuesTwaekHelper.EnemyValues myEnemyValues;
    GameValuesTwaekHelper gameValuesTwaekHelper;
    // Start is called before the first frame update
    void Start()
    {
        gameValuesTwaekHelper = ReferenceMaster.instance.gameValuesTwaekHelper;
        myEnemyValues = gameValuesTwaekHelper.GetEnemyValues(enemyType);
        EnemyShooting enemyShooting = GetComponent<EnemyShooting>();
        enemyShooting.SetBulletDamage(myEnemyValues.damage);
        enemyShooting.SetBulletSpeed(myEnemyValues.bulletSpeed);
        enemyShooting.SetFiringInvervalFromAttackPerSec(myEnemyValues.attackPerSec);
        enemyShooting.SetInitialDealy(myEnemyValues.initialDeayToShoot);
        GetComponent<EnemyHealth>().SetEnemyHealth(myEnemyValues.health);
        SetMovementSpeedValues(myEnemyValues.movementSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetMovementSpeedValues(float moveSpeed)
    {
        
        EnemyPathFinding enemyPathFinding = GetComponent<EnemyPathFinding>();
        EnemyTypeBasedOnCovering enemyTypee = enemyPathFinding.enemyType;
        switch (enemyTypee)
        {
            case EnemyTypeBasedOnCovering.moving:
                {
                    if (enemyPathFinding.rpg)
                    {
                        enemyPathFinding.walkSpeed = moveSpeed;
                    }
                    else if (enemyPathFinding.lmg)
                    {
                        enemyPathFinding.walkSpeed = moveSpeed;
                    }
                    else if (enemyPathFinding.humvee)
                    {
                        enemyPathFinding.walkSpeed = moveSpeed;
                        GetComponent<HumveeBehavior>().walkSpeed = moveSpeed;
                    }
                    else if (enemyPathFinding.tripod)
                    {
                        enemyPathFinding.walkSpeed = moveSpeed;
                    }
                    else if (enemyPathFinding.drone)
                    {
                        enemyPathFinding.walkSpeed = moveSpeed;
                        GetComponent<DroneBehavior>().walkSpeed = moveSpeed;
                    }
                    else if (enemyPathFinding.bossSwat)
                    {
                        enemyPathFinding.walkSpeed = moveSpeed;
                        GetComponent<MoverToAnyPoint>().walkSpeed= moveSpeed;
                    }
                    else if (enemyPathFinding.bossTank)
                    {
                        enemyPathFinding.walkSpeed = moveSpeed;
                        GetComponent<MoverToAnyPoint>().walkSpeed = moveSpeed;
                    }
                    else
                    {
                        enemyPathFinding.walkSpeed = moveSpeed;
                    }
                    break;
                }
            case EnemyTypeBasedOnCovering.covering:
                {
                    enemyPathFinding.walkSpeed = moveSpeed;
                    break;
                }

        }
    }
}
