%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: BossSwatAvatarMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Bind_Hips
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_LeftUpLeg
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_LeftUpLeg/Bind_LeftLeg
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_LeftUpLeg/Bind_LeftLeg/Bind_LeftFoot
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_LeftUpLeg/Bind_LeftLeg/Bind_LeftFoot/Bind_LeftToeBase
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_LeftUpLeg/Bind_LeftLeg/Bind_LeftFoot/Bind_LeftToeBase/Bind_LeftToe_End
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_RightUpLeg
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_RightUpLeg/Bind_RightLeg
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_RightUpLeg/Bind_RightLeg/Bind_RightFoot
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_RightUpLeg/Bind_RightLeg/Bind_RightFoot/Bind_RightToeBase
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_RightUpLeg/Bind_RightLeg/Bind_RightFoot/Bind_RightToeBase/Bind_RightToe_End
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand/Bind_LeftHandIndex1
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand/Bind_LeftHandIndex1/Bind_LeftHandIndex2
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand/Bind_LeftHandIndex1/Bind_LeftHandIndex2/Bind_LeftHandIndex3
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand/Bind_LeftHandIndex1/Bind_LeftHandIndex2/Bind_LeftHandIndex3/Bind_LeftHandIndex4
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand/Bind_LeftHandThumb1
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand/Bind_LeftHandThumb1/Bind_LeftHandThumb2
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand/Bind_LeftHandThumb1/Bind_LeftHandThumb2/Bind_LeftHandThumb3
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand/Bind_LeftHandThumb1/Bind_LeftHandThumb2/Bind_LeftHandThumb3/Bind_LeftHandThumb4
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Bind_HeadTop_End
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand/Bind_RightHandIndex1
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand/Bind_RightHandIndex1/Bind_RightHandIndex2
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand/Bind_RightHandIndex1/Bind_RightHandIndex2/Bind_RightHandIndex3
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand/Bind_RightHandIndex1/Bind_RightHandIndex2/Bind_RightHandIndex3/Bind_RightHandIndex4
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand/Bind_RightHandThumb1
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand/Bind_RightHandThumb1/Bind_RightHandThumb2
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand/Bind_RightHandThumb1/Bind_RightHandThumb2/Bind_RightHandThumb3
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand/Bind_RightHandThumb1/Bind_RightHandThumb2/Bind_RightHandThumb3/Bind_RightHandThumb4
    m_Weight: 1
  - m_Path: Mesh_GRP
    m_Weight: 0
  - m_Path: Mesh_GRP/Mesh_Boss1
    m_Weight: 0
  - m_Path: Mesh_GRP/Sheild
    m_Weight: 0
  - m_Path: Mesh_GRP/SM_Wep_Rifle_Assault_04
    m_Weight: 0
  - m_Path: Mesh_GRP/SM_Wep_Rifle_Assault_04/SM_Wep_Rifle_Assault_Flash_04
    m_Weight: 0
  - m_Path: Mesh_GRP/SM_Wep_Rifle_Assault_04/SM_Wep_Rifle_Assault_Mag_04
    m_Weight: 0
  - m_Path: Mesh_GRP/SM_Wep_Rifle_Assault_04/SM_Wep_Rifle_Assault_Slide_04
    m_Weight: 0
  - m_Path: Mesh_GRP/SM_Wep_Rifle_Assault_04/SM_Wep_Rifle_Assault_Stock_04
    m_Weight: 0
  - m_Path: Mesh_GRP/SM_Wep_Rifle_Assault_04/SM_Wep_Rifle_Assault_Trigger_04
    m_Weight: 0
