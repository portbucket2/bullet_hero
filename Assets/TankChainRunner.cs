﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankChainRunner : MonoBehaviour
{
    public GameObject[] chainUnits;
    public Vector3[] chainUnitsPosSaved;
    public Quaternion[] chainUnitsRotSaved;
    public float progression;
    public float speed;
    float speedVar;
    public bool left;
    // Start is called before the first frame update
    void Start()
    {
        chainUnitsPosSaved = new Vector3[chainUnits.Length];
        chainUnitsRotSaved = new Quaternion[chainUnits.Length];
        for (int i = 0; i < chainUnits.Length; i++)
        {
            chainUnitsPosSaved[i] = chainUnits[i].transform.localPosition;
            chainUnitsRotSaved[i] = chainUnits[i].transform.localRotation;
        }

        //GetComponentInParent<MoverToAnyPoint>(). ActionStationLeave 
        GetComponentInParent<MoverToAnyPoint>().ActionStationReach += ChainStop;
        GetComponentInParent<BossTankRotationControl>().ActionRotStart += ChainRunDirDetermine;
        GetComponentInParent<BossTankRotationControl>().ActionRotEnd += ChainRun;
        speed = ReferenceMaster.instance.gameValuesTwaekHelper.bossTank.movementSpeed * 7 / 5;
        ChainRun();
    }

    // Update is called once per frame
    void Update()
    {
        AdvanceChain();
    }
    public void AdvanceChain()
    {
        if(speedVar == 0)
        {
            return;
        }
        progression += speedVar * Time.deltaTime;
        if(progression >= 1)
        {
            progression -= 1;
        }
        if(progression <= 0)
        {
            progression += 1;
        }
        for (int i = 0; i < chainUnits.Length; i++)
        {
            GameObject nextUnit = null;
            int nxtUnitIndex = 0;
            if(i == chainUnits.Length -1)
            {
                nextUnit = chainUnits[0];
                nxtUnitIndex = 0;
            }
            else
            {
                nextUnit = chainUnits[i + 1];
                nxtUnitIndex = i + 1;
            }
            chainUnits[i].transform.localPosition = Vector3.Lerp(chainUnitsPosSaved[i], chainUnitsPosSaved[nxtUnitIndex] , progression);
            chainUnits[i].transform.localRotation = Quaternion.Lerp(chainUnitsRotSaved[i], chainUnitsRotSaved[nxtUnitIndex] , progression);
        }
    }

    public void ChainRun()
    {
        
        speedVar = speed;
    }
    public void ChainRun(float speedd)
    {
        speed = speedd;
        speedVar = speed;
    }
    public void ChainStop()
    {
        speedVar = 0;
    }
    public void ChainRunDirDetermine()
    {
        float rotNed = GetComponentInParent<BossTankRotationControl>().rotNeeded;
        if(rotNed > 180)
        {
            rotNed = -(360 - rotNed);
        }
        else if(rotNed < -180)
        {
            rotNed = (360 - Mathf.Abs(rotNed));
        }
        float negFac = 1;
        if (!left)
        {
            negFac = -1;
        }
        if(rotNed > 0)
        {
            speedVar = speed * negFac;
        }
        else if (rotNed < 0)
        {
            speedVar = -speed * negFac;
        }
        else
        {
            speedVar = speed;
        }
    }
}
